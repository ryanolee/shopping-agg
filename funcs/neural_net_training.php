<?php
/**
 * This document defines functions that assist in training the neural network handling entity resolution of multi source items .
 */

/**
 * This namspace deals with functions that assist in training andy given neural network in entity resolution (for)
 */
namespace shopping_agg\NeuralNetwork\train;
//require("./../exturnal_deps/neural_net/neural_net.php");
/**
 * This function links data between two arrays ($a1 and $a2) from a mapping array $map
 * @param array $map must be in form array([x]=>array(["id1"]=>["id_val1"],["id2"]=>["id_val2"]))
 * @param array $a1 must be in form array([x]=>array(["id1"]=>["id_val1"]))
 * @param array $a2 must be in form array([x]=>array(["id2"]=>["id_val2"]))
 * @return array $data in the form array(array([MATCHED ITEM 1]),array([MATCHED ITEM 2]))
 */
//GWAAAAA MY EYYYS THEY BUUUURN
function map_data($map,$a1,$a2){
	return array_map(function($entry_to_map)use($a1,$a2){//iterate through all the id(s)
		$entry_to_map_keys=array_keys($entry_to_map);//given that entry to map in in the form $array(["id1"]=>["id_val"],["id2"]=>["id_val2"])
		foreach (isset($a1[0][$entry_to_map_keys[0]])?$a1:$a2 as $da1){//iterate through the array containing the first id as a valid array key
			if($entry_to_map[$entry_to_map_keys[0]]==$da1[$entry_to_map_keys[0]]){//if there is a match between the value of the id and the value of the id in the data
				$data_1=$da1;//set the first part of the binding to the formatted data
				break;//escape the search
			}
		}
		foreach (isset($a1[0][$entry_to_map_keys[1]])?$a1:$a2 as $da1){//do the same for the second array
			if($entry_to_map[$entry_to_map_keys[1]]==$da1[$entry_to_map_keys[1]]){
				$data_2=$da1;
				break;
			}
		}
		return array($data_1,$data_2);
	},$map);
}

/**
 * Gets test data from csv files for training neural networks
 * @return array $array test_data the 
 */
function get_training_data(){
	$prev_dir=getcwd();//get current dir
	chdir(__DIR__);//change to dir where doc is defined
	if(file_exists("./NNTD/DELETE_ME_ON_UPDATE_OF_DATA.txt")){//if processing has allready occoured
		echo "data exsists at ./NNTD/DELETE_ME_ON_UPDATE_OF_DATA.txt loading from there.<br/>";
		$data=unserialize(file_get_contents("./NNTD/DELETE_ME_ON_UPDATE_OF_DATA.txt"));//get data
		chdir($prev_dir);//reset directory
		return $data;//return updated data
	}
	$data=array(//define test data locations (NNTD=> Neural Network Training Data)
			"abt"=>"./NNTD/Abt.csv",
			"abt_buy"=>"./NNTD/Abt_Buy_mappings.csv",
			"buy"=>"./NNTD/Buy.csv",
			"amazon"=>"./NNTD/Amazon.csv",
			"amazon_google"=>"./NNTD/Amazon_Google_mappings.csv",
			"google"=>"./NNTD/GoogleProducts.csv"
	);
	$data=array_map(//map data
			function($value){//using this callback
				echo "Getting data from $value <br/>";
				$value=explode("\n",file_get_contents($value));//get file contents and split it
				echo "Formatting data... <br/>";
				$headers=array_shift($value);//get first value
				$headers=str_getcsv($headers,",");//break apart headers
				$value=array_map(function($data)use($headers){//update array mappings 
					$new_array=[];//create a new array
					$data=str_getcsv($data,",");//break apart single entery into array
					for($i=0; $i<count($data); $i++){//iterate through array by...
					$new_array=array_merge($new_array,array($headers[$i]=>$data[$i]));//mapping relavent index key to array key
					
				}
				return $new_array;//map new entry over old string entery
				},$value);
				echo "done <br/>";
				return $value;
			},
			$data
	);//replace file names with there contents
	echo "Mapping data...<br/>";
	$data=array(
			"abt_buy"=>map_data($data["abt_buy"], $data["abt"], $data["buy"]),
			"amazon_google"	=>map_data($data["amazon_google"], $data["google"], $data["amazon"])
	);
	echo "Done!<br/>";
	//file_put_contents("./test.txt",var_export($data,true)); //DEBUG
	echo "backing up data...<br/>";
	file_put_contents("./NNTD/DELETE_ME_ON_UPDATE_OF_DATA.txt", serialize($data));//save data so does not has to be processed again
	echo "Done!<br/>";
	chdir($prev_dir);//reset dir
	return $data;
}
/**
 * Gets a random array from a given dataset.
 * @param array $data Must be an array in the form array([x]=>array([associative array of data 1],[associative array of data 2]))
 * @param bool $match if the pairing of data should match or not
 * @see \NeuralNetwork\train::get_training_data
 */
function get_rand_pair(&$data,$match=true){//data passed as reference due to its size
	$index_1=mt_rand(0,count($data)-1);//get a random index between 0 and the size of the passed array
	if($match){
		if($data[$index_1][0]["price"]==""||$data[$index_1][1]["price"]==""||$data[$index_1][0]["description"]==""||$data[$index_1][1]["description"]==""){
			return get_rand_pair($data,$match);//try again if data has no price
		}
		return $data[$index_1];//return ramdom matching pair
	}
	$index_2=mt_rand(0,count($data)-1);
	while($index_2===$index_1){//Force the indexes to be diffrent
		$index_2=mt_rand(0,count($data)-1);//lets go for that 1 in a googolplexian chance that this takes a million years to resolve. X-X
	}
	if($data[$index_1][0]["price"]==""||$data[$index_2][1]["price"]==""||$data[$index_1][0]["description"]==""||$data[$index_2][1]["description"]==""){
		return get_rand_pair($data,$match);//try again if data has no price or description
	}
	return array($data[$index_1][0],$data[$index_2][1]);//return a non matching pair
}
/**
 * This constant holds the default path (absolute only) to where the en_GB.dict file is loacated (https://wiki.mozilla.org/L10n:Dictionaries)
 * @var string
 */
define("ENCHANT_DICT_PATH","C:\wamp\bin\php\php7.0.3\share\myspell\dicts\\");
/**
 * 
 * This function eliminates all common english words from a string (REQUIRES enchant to be enabled)
 * @param string $string The string to remove text from
 * @return string The passed string with no english words
 * @see shopping_agg\NeuralNetwork\train::eliminate_english_words(string $string) 
 */
function eliminate_english_words_enchant($string){
	if(!function_exists("enchant_broker_init")){
		//\shopping_agg\error_handle\error_log_c("Error: call made to NeuralNetwork\train\eliminate_english_words without enchant being enabled. Please refer to http://php.net/manual/en/ref.enchant.php for further information");
		return $string;
	}
	if(!isset($GLOBALS["neural_net"]["dict"])){//if a dictionary is not defined (because they take up a large amount of memory)
		$broker=enchant_broker_init();//initilize enchant broker
		enchant_broker_set_dict_path ($broker,ENCHANT_MYSPELL,ENCHANT_DICT_PATH);
		if(!enchant_broker_dict_exists($broker,'en_GB')){//check for english dictionary
			\shopping_agg\error_handle\error_log_c("Error: no dictionary with the tag of 'en_US' was found as part of the enchant module.");//log error
			return $string;//return original string
		}
		$GLOBALS["neural_net"]["dict"]=enchant_broker_request_dict($broker, 'en_GB');//get a dictionary and save it to a global (to save memory)
	}
	
	
	return implode(" ", //join scentence back together
		array_map(function($word){
			
			preg_match("/^[[:punct:]]*(?<word>\w+)[[:punct:]]*$/i", $word, $data);//strip punctuation from word (so stuff like "hello." turns into "hello".)
			if(is_array($data)&&count($data)!==0){//if there was stripped punctuation
				$word=$data["word"];//set the word to the stripped form of itself
			}
			if(strlen($word)<=1){
				return "";
			}
			if(enchant_dict_check($GLOBALS["neural_net"]["dict"], $word)){
				return "";//remove term if it is a word
			}
			
			return $word;//otherwise keep the unique term
		},
			explode(" ",$string)//apply this to array of words.
		)
	);
}
/**
 * 
 * This function eliminates all common english words from a string without the need for the encant module to be enabled (but alot slower)
 * @param string $string The string to remove text from
 * @return string The passed string with no english words
 * @see \shopping_agg\NeuralNetwork\train::eliminate_english_words_enchant()
 */
function eliminate_english_words($string){
	if(!\file_exists(__DIR__."/words/words.txt")){
		\shopping_agg\error_handle\error_log_c("Error: no dictionary file found at ".__DIR__."/words/words.txt");
		return $string;
	}
	if(!isset($GLOBALS["neural_net"]["dict_word"])){//if a dictionary is not defined (because they take up a large amount of memory)
		$GLOBALS["neural_net"]["dict_word"]=explode("\n", file_get_contents(__DIR__."/words/words.txt"));
	}
	return implode(" ", //join scentence back together Copy paste 
			array_map(function($word){	
				preg_match("/^[[:punct:]]*(?<word>\w+)[[:punct:]]*$/i", $word, $data);//strip punctuation from word (so stuff like "hello." turns into "hello".)
				if(is_array($data)&&count($data)!==0){//if there was stripped punctuation
					$word=$data["word"];//set the word to the stripped form of itself
				}
				if(strlen($word)==0){
					return "";
				}
				if(strlen($word)==1){
					if(in_array( strtolower($word),explode("\n", file_get_contents(__DIR__."/words/less_than_1.txt")))){//load text file for words with engths less than 1
						return "";
					}
				}
				if(in_array( strtolower($word),(file_exists(__DIR__."/words/".substr($word, 0,2).".txt")?(explode("\n",file_get_contents(__DIR__."/words/".substr($word, 0,2).".txt"))):(array())))){// if file exsist load word list and check if word is in it and otherwise skip it.
					return "";//remove term if it is a word
				}
					
				return $word;//otherwise keep the unique term
			},
			explode(" ",$string)//apply this to array of words.
			)
			);
}
/**
 * This genarates a random number if training vectors from the dataset given by get_training_data
 * @param number $number_of_vectors The number of traing vectors to genarate.
 * @param float $chance_of_bieng_a_match The decimal chance of a match being selected out of the dataset.
 * @return array $data in the form array([input nodes],[expected output])
 * @see \NeuralNetwork\train::get_training_data
 */

function genarate_training_vectors($number_of_vectors=1000,$chance_of_being_a_match=0.5){
	$data=get_training_data();//get the training data
	echo "Genarating training vectors...<br/>";
	$number_of_vectors-=2;//correct bodged loops
	if(!is_float($chance_of_being_a_match) || @0>=$chance_of_being_a_match||@$chance_of_being_a_match>1){
		echo "Error: Chance of bieng a match has been passed as an invalid value; reseting to a 50/50 ratio.";
		$chance_of_being_a_match=0.5;
	}
	//genarate training vectors
	$array_of_vectors=[];
	foreach(array_keys($data) as $key ){//get array keys and iterate through them
		for($c=0;$c<=round($number_of_vectors/count($data));$c++){//for each data array
			$match=($chance_of_being_a_match>mt_rand(0,100)/100)?1:0;//randomly select if the vector will be a match or not between the defined decimal or not
			$array_of_vectors[]=array(get_rand_pair($data[$key],$match),$match);//genarate matches based off dataset
		}	
	}
	echo "Done!<br/>";
	echo "Resolving array of vectors to numbers<br/>";
	shuffle($array_of_vectors);//mix the data sources randomly
	$final_set_of_test_data_unnormalized=[];//create un normalized array of data
	foreach ($array_of_vectors as $key=>$vector){//for each vector in the un normalized array
		echo (string)round(((int)$key/$number_of_vectors)*100,1)."%<br/>";//print percentage complete
		$resolved=evaluate_vector($vector[0][0], $vector[0][1]);//get the data into an unormalized state
		$final_set_of_test_data_unnormalized[]=array($resolved,$vector[1]);//and store it with its pre-requsite output
	}
	echo "Done!<br/>";
	//echo var_export($array_of_vectors,true);
	//echo var_export($final_set_of_test_data,true);
	$min_max_data=$final_set_of_test_data_unnormalized[0][0];//get baselie reading of un normalized data
	$min_max_data=array_map(function($item){return array($item,$item);},$min_max_data);//copy baseline for min max values
	//var_export($min_max_data);
	foreach($final_set_of_test_data_unnormalized as $entry){//for each entry in the un notmalized data
		$entry=$entry[0];//ignore qutput as a result
		//var_dump($entry);
		foreach (range(0,count($min_max_data)-1,1) as $x){
			//CALCULATE THE MIN AND VALUES FOR BOTH (in the form array(x=>array($min,$max)))
			if($min_max_data[$x][0]>$entry[$x]){
				//echo "min updated to".(string)$entry[$x];
				$min_max_data[$x][0]=$entry[$x];
			}
			if($min_max_data[$x][1]<$entry[$x]){
				//echo "max updated to".(string)$entry[$x];
				$min_max_data[$x][1]=$entry[$x];
			}
		}
	}
	$final_set_of_test_data=[];//define final test dataset
	$final_set_of_test_data=array_map(function($vector)use($min_max_data){//map array to data
		//echo(var_dump($vector));
		$output=$vector[1];
		$vector=$vector[0];
		foreach (range(0,count($vector)-1) as $x){
			$vector[$x]=($vector[$x]-$min_max_data[$x][0])/($min_max_data[$x][1]-$min_max_data[$x][0]);//I = Imin + (Imax-Imin)*(D-Dmin)/(Dmax-Dmin) Normalize data
		}
		return array($vector,array($output));//return array with output
		
	},$final_set_of_test_data_unnormalized);
	//echo var_export($final_set_of_test_data);
	
	return $final_set_of_test_data;
}

/**
 *  Taken from http://php.net/manual/en/function.similar-text.php
 * @param string $str1 
 * @param string $str2
 * @return number
 * @link http://php.net/manual/en/function.similar-text.php
 */
function similarity($str1, $str2) {
	error_reporting(0);
	$len1 = strlen($str1);
	$len2 = strlen($str2);
	if($len1==0||$len2==0){
		return 0;
	}
	$max = max($len1, $len2);
	$similarity = $i = $j = 0;
	while (($i < $len1) && isset($str2[$j])) {
		if ($str1[$i] == $str2[$j]) {
			$similarity++;
			$i++;
			$j++;
		} elseif ($len1 < $len2) {
			$len1++;
			$j++;
		} elseif ($len1 > $len2) {
			$i++;
			$len1--;
		} else {
			$i++;
			$j++;
		}
	}
	error_reporting(E_ALL);
	return round($similarity / $max, 2);
}
/**
 * This converts the $item to human readable data to neural network based testing
 * @param array $item1 an assicitive array with the indexes of ["price"],["title"] and ["description"]
 * @param array $item2 an assicitive array with the indexes of ["price"],["title"] and ["description"]
 * @return number[]
 */
function evaluate_vector($item1,$item2){
	return array(
			//ITEM 1
			//floatval(str_replace("$", "", $item1["price"])),//price of item 1
			//ITEM 2
			//floatval(str_replace("$", "",$item2["price"])),//price of item 2
			//BOTH
			abs(floatval(str_replace("$", "",$item1["price"]))-floatval(str_replace("$", "",$item2["price"]))),//diffrence in price
			similarity($item1["title"],$item2["title"]),//similatity of title
			similarity(eliminate_english_words($item1["title"]),eliminate_english_words($item2["title"])),//sililarity of title without english words
			similarity($item1["description"],$item2["description"]),//similarity of description
			similarity(eliminate_english_words($item1["description"]),eliminate_english_words($item2["description"]))//similarity of description without english words
			
	);
}

ini_set('max_execution_time', 0);
ini_set('memory_limit','2048M');
$data=genarate_training_vectors(2000);
//var_export($data);
//exit();
$net=new \NeuralNetwork\NeuralNetwork(count($data[0][0]),5,2,1);
$net->setVerbose(false);
$net->setLearningRate(0.1);
$net->setMomentum(0.85);

foreach($data as $item){
	$net->addTestData($item[0],$item[1]);
}
$net->train(1000000,0.1);
$net->save("Test_net.txt");
//var_dump( eliminate_english_words("Sale rgx_910 here! sony npr c 300 cost sdehufgasjdhgkajsdhfgahsdghaedjadhgajhdshSDKFHASKJDHGAKDG"));