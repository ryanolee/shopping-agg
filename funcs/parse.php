<?php
/**
 * This document defines the group of functions that handle the convertion and parsing of JSON to the interactional api layer.In conjunction with other general parsing functions
 * @author rizza <rizza@rizza.net>
 * @package shopping_aggregator\functions\parse
 * @since v1.0
 */
/**
 * Docblock to separate docblock above from code below (@link http://stackoverflow.com/questions/21312643/phpdoc-no-summary-found-for-this-file)
 */
namespace shopping_agg\parse;



/**
 * Converts both xml and json (from a string) into a standard associative array.
 * @param string $data The string to convert into an array structure
 * @return array|bool Returns asccociative array of data when needed and false on error.
 */

function xml_and_json_to_assoc_array($data){
	$xml=@simplexml_load_string($data);//try xml conversion (supress error on failure)
	$data=($xml)?json_encode($xml):json_encode($data);//convert either the valid xml or the original string into json
	$data=json_decode($data,true);//decode json to array
	if(!$data){//if data threw error
		\shopping_agg\error_handle\error_log_c("Error: unable to parse data as passed data as either JSON or XML. ",$data);//log error
		return false;//return error
	}
	return $data;
}

/**
 * allias of the json decode function with the ability to decode json with padding
 * @see \json_decode
 * @param unknown $jsonp The json with padding to use
 * @param string $assoc
 * @return mixed
 */
function jsonp_decode($jsonp, $assoc = false) { // PHP 5.3 adds depth as third parameter to json_decode
	if($jsonp[0] !== '[' && $jsonp[0] !== '{') { // we have JSONP
		$jsonp = substr($jsonp, strpos($jsonp, '('));
	}
	return json_decode(trim($jsonp,'();'), $assoc);
}
/**
 * convets a valid associateve array into a endpoint object
 * @param array $endpoint The endpoint array to convert
 * @param array $override The overide for the url
 * @return \shopping_agg\interaction\endpoint
 */
function assoc_array_to_endpoint($endpoint,$override=array()){
	return new \shopping_agg\interaction\endpoint(null,
			(isset($override["url"])&&$endpoint["endpoint"]==="")?$override["url"]:$endpoint["endpoint"],
			(isset($endpoint["method"]))?$endpoint["method"]:"GET",
			(isset($endpoint["POST_data"]))?$endpoint["POST_data"]:array(),
			(isset($endpoint["GET_data"]))?$endpoint["GET_data"]:array(),
			(isset($endpoint["data"]))?$endpoint["data"]:array(),
			(isset($endpoint["API_key"]))?$endpoint["API_key"]:"DEFAULT",
			(isset($endpoint["pre_request_function"]))?$endpoint["pre_request_function"]:"",
			(isset($endpoint["delay"]))?$endpoint["delay"]:0,
			(isset($endpoint["tries"]))?$endpoint["tries"]:1
			);
}
/**
 * convets a valid associateve array into a interactional object
 * @param unknown $interaction
 * @param array $override The overide for the url
 * @param integer $depth the depth of recourtion 
 * @return boolean|\shopping_agg\interaction\interaction
 */
function assoc_array_to_interactional_object($interaction,$override=array(),$depth=0){
	if(is_string($interaction)){
		$interaction=json_decode($interaction,true);
		if(!$interaction){//if error
			\shopping_agg\error_handle\error_log_c("Error: failed to parse json. \n Invalid syntax. Error returned as:\n ".var_export(json_last_error_msg(),true));//log error
			return false;
		}
	}
	\shopping_agg\debug\debug_print("Building endpoint...");
	$endpoint=$interaction["request"];
	$endpoint=assoc_array_to_endpoint($endpoint,$override);
	//FIXME AFTER RESPONESE CLASS IS BUILT
	\shopping_agg\debug\debug_print("Done!");
	\shopping_agg\debug\debug_print("Building response...");
	$response=$interaction["response"];
	$response=assoc_array_to_interactional_response($response,$depth+1,$response["response_type"]);//send type on each new api type variation
	\shopping_agg\debug\debug_print("Done!");
	\shopping_agg\debug\debug_print("Building errors...");
	\shopping_agg\debug\debug_print("Building Done!");
	$errors=null;
	//FIXME AFTER RESPONESE CLASS IS BUILT
	return new \shopping_agg\interaction\interaction(null,$endpoint,$response,$errors);
}
/**
 * This function converts an associative array containing information about the response object into a response class instance
 * @param array $response the response data
 * @param integer $depth the depth of recourtion 
 * @param string $type (the type of response)
 * @return \shopping_agg\interaction\response $response the response object
 * 
 */
function assoc_array_to_interactional_response($response,$depth=0,$type="",$pivot=""){
	//$response_back=$response;//back up response
	array_walk($response["vars_to_look_for"],function(&$item,$key)use($depth,$type){//build interactional objects recoursivly
		if(isset($item["selector"])&&isset($item["goto"])){//if interactractional object is required
			\shopping_agg\debug\debug_print("Building interaction from chained object[$key]...");
			$item["goto"]["request"]["url"]="http://www.if-you-see-this-then-the-response-chaining-has-failed.com";
			if(DEBUGGING){
				echo "<div style=' text-indent: ".(string)50*($depth+1)."px;'>";
			}
			$item["goto"]=assoc_array_to_interactional_object($item["goto"],array(),$depth+1);//make recoursive call
			if(DEBUGGING){
	
				echo "</div>";
			}
		}
		elseif(isset($item["pivot"])){
			\shopping_agg\debug\debug_print("Building response from chained object[$key]...");
			if(DEBUGGING){
				echo "<div style=' text-indent: ".(string)50*($depth+1)."px;'>";
			}
			$piv=$item["pivot"];//grab the pivot
			unset($item["pivot"]);//kill the rest
			$item["vars_to_look_for"]= array_merge(array(),$item);//update refrence for next recoursive call. No circular references by way of cloning the data. note the clone function only works on objects.
			$item=assoc_array_to_interactional_response($item,$depth+1,$type,$piv);//make recoursive call
			if(DEBUGGING){
				echo "</div>";
			}
		}
	});
	$response=new \shopping_agg\interaction\response(
		(strlen($type)!=0)?$type:$response["response_type"],
		(strlen($pivot)!=0)?
			$pivot:
			(isset($response["pivot"])?$response["pivot"]:""),//favour overide pivot then pivot then an emprey string 
		isset($response["vars_to_look_for"])?$response["vars_to_look_for"]:array()
	);
	foreach($response->vars_to_look_for as $var){
		//var_dump($var);
		if(is_a($var,"shopping_agg\api\interactinal_class")){
			$var->remap_master($response);//update response references
		}
	}
	return $response;
}

/**
 * This function is the main parser for json objects as they are converted into the interactional layer.
 * @param string $data
 * @return boolean|\shopping_agg\api\api
 */
function json_to_interactional_layer($data,$interactional_object=false){
	if (file_exists($data)){// if the pased data is a file
		\shopping_agg\debug\debug_print("Call made to \shopping-agg\parse::json_to_interactional_layer looking in file '$data' for json to parse ");
		$data=file_get_contents($data);//replace it with the files contents
	}
	$data=json_decode($data);//decode the passed json
	if(!$data){//if error
		\shopping_agg\error_handle\error_log_c("Error: failed to parse json. \n Invalid syntax. Error returned as:\n ".var_export(json_last_error_msg(),true));//log error
		return false;
	}
	
	require_once __DIR__.'/../exturnal_deps/json_validator/vendor/autoload.php';
	\shopping_agg\debug\debug_print("Validating json against schema...");
	$validator = new \JsonSchema\Validator;
	$validator->check((object)$data, (object)['$ref' =>__DIR__."/../json/schema/core.json"]);
	if ($validator->isValid()) {
		\shopping_agg\debug\debug_print("JSON is valid and checks against the validation schema.");
	} else {
		\shopping_agg\debug\debug_print( "JSON is invalid:");
		foreach ($validator->getErrors() as $error) {
				\shopping_agg\debug\debug_print(" <b>[". (($error['property']==="")?"root_node":$error['property'])."]: </b>". $error['message']);
			}
		return false;
	}
	$data=json_decode(json_encode($data), true);//convert object tree to multidimentional associative array.
	\shopping_agg\debug\debug_print("Validation success! building objects for ".$data["API_name"]."...");
	//BUILD API KEYS
	if(@count($data["api_keys"])==0&&$data["API_requires_auth_key"]){//if there are no api keys when there should be
		\shopping_agg\debug\debug_print("Warning: API_requires_auth_key set to true and no auth keys_have been defined. Assuming API_requires_auth_key is false.");
		$data["API_requires_auth_key"]=false;
	}
	if((isset($data["API_requires_auth_key"]))?$data["API_requires_auth_key"]:false){//if auth_key is defined and true
		\shopping_agg\debug\debug_print("Building api key objects...");
		$api_keys=array_map(function($api_key_object){
			\shopping_agg\debug\debug_print("Building api key ['".(string)$api_key_object["name"]."']",$api_key_object);//print api key genaration
			return new \shopping_agg\api\api_key(null,//build array of api key objects
					$api_key_object["name"],//name allways defined
					$api_key_object["value"],//value allways defined
					((isset($api_key_object["method"]))?$api_key_object["method"]:"GET"),//allow for variable to not be defiend
					((isset($api_key_object["default_key"]))?$api_key_object["default_key"]:false));//allow for variable to be set to false
		},$data["api_keys"]);
		\shopping_agg\debug\debug_print("Building api_keys object...");
		$api_keys=new \shopping_agg\api\api_keys(null,$api_keys);//build the api keys
		\shopping_agg\debug\debug_print("Done.");
	}
	//BUILD SEARCH @todo add search and respones parsing
	\shopping_agg\debug\debug_print("Building search object...");
	$search=$data["search"];
	$search=assoc_array_to_interactional_object($search);
	if(!$search){
		\shopping_agg\debug\debug_print("Failed in creating search object.");
		return false;
	}
	if(isset($data["next"])){
		\shopping_agg\debug\debug_print("Building next from search object ...");
		$next=unserialize(serialize($search));//make copy of search object
		if(!$next->set_endpoint(assoc_array_to_endpoint($data["next"]))){//update endpoint and check it is ok
			\shopping_agg\debug\debug_print("Failed in duplicating and remapping the search object to the next paramiter..");//if not
			return false;//throw error
		}
		\shopping_agg\debug\debug_print("Done.");
	}
	\shopping_agg\debug\debug_print("Done.");
	\shopping_agg\debug\debug_print("Building API...");
	$api=new \shopping_agg\api\api(
			$data["API_name"],
			isset($data["API_image"])?$data["API_image"]:"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAMAAAAIG46tAAAAD1BMVEX///9/f38AAAC/v7//AADv/1gRAAAARUlEQVQokWNgxA0YGIAINxiskuieQJVEUs/MRD9JFhYIxq6ThQUkh8tYsBwZOvHbSRt/4pZkRgLokkwoAE0SOxh0kvgAAPYBAR8esxZzAAAAAElFTkSuQmCC",
			$search,
			(isset($data["API_requires_auth_key"]))?$data["API_requires_auth_key"]:false,
			null,
			(isset($api_keys))?$api_keys:null,
			isset($next)?$next:null
			);
	\shopping_agg\debug\debug_print("Done.<br/><b style='color:green'>Parsing complete</b>");
	return $api;
}
//gotten from: https://www.codeproject.com/Questions/553031/JSONplusTOplusXMLplusconvertionpluswithplusphp
/**
 * converts array to XML.
 * @param array $template_info the array to pass as data for
 * @param \SimpleXMLElement $xml_template_info  the template info
 */
function array_to_xml($array, &$xml) {
	//var_dump($array);
    foreach($array as $key => $value) {
        if(is_array($value)) {
            if(!is_numeric($key)){
                $subnode = $xml->addChild("$key");
                array_to_xml($value, $subnode);
            } else {
                array_to_xml($value, $xml);
            }
        } else {
            $xml->addChild("$key",("$value"!=="")?htmlspecialchars("$value"):trim(var_export($value,true),"'"));
        }
    }
}