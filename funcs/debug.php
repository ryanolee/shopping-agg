<?php
/**
 * This document defines all the required debug methods for ease of debugging during script execution
 * @author rizza <rizza@rizza.net>
 * @package shopping_aggregator\functions\debugging
 */

namespace shopping_agg\debug;
use interaction\response;
/**
 * This function handles debug printouts for the entirity of the spript
 * @param string $to_print the string to print to screen
 * @param string ...$extra_data the extra data to show during deep debugging;
 */
function debug_print($to_print,...$extra_data){
	
	if(DEBUGGING){
		if(ob_get_level()===0){
			ob_start();
			
		}
    	echo "<div style='all: revert;background-color:grey;'>".str_ireplace( "\n", "<br/>",$to_print)."</div>";
    	if(DEEP_DEBUGGING){
	    	if(!empty($extra_data)){//Add edge case for logged errors
				echo "<br/>RELATED VARIABES:<br/><pre>";
				foreach($extra_data as $data){
					var_dump($data);
				}
				echo "</pre></div><hr/>";
			}
    	}
    	
    	ob_flush();
    	flush();
	}
}
/**
 * This prints html into an iframe using jsvascript on the webpage.
 * @param unknown $html the html to print (dubugging must be enabled)
 */
function debug_iframe_print($html){
	if(DEBUGGING){
		$id=sha1(rand());
		echo "<hr>
		<iframe id='$id' width='100%' height='640'></iframe>
		<script>
			
		var doc = document.getElementById('$id').contentWindow.document;
		doc.open();
		doc.write(".json_encode(array("data"=>$html))."['data']);
						  doc.close();
						</script></hr> ";
	}
}
/**
 * This tests a set of arguents against a set of objects an gives an appropreate printout.
 * Debugging must be set to true for this function to work.
 * @see DEBUGGING
 * @param string $function The name of the function $function the function or method to call.
 * @param array $args the arguments to pass to the function
 * @param mixed $object An object to test the defined method on. 
 * @return string $data (This is the csv data line that holds the debug data)
 */
function debug_test($function="",$args=array(),$expected_outcome="Nothing",$object=NULL){
	if (DEBUGGING){
		if(!is_string($function)){
			\shopping_agg\error_handle\error_log_c("Error: function not passed as string; returning false.",$function);
			return false;
		}
		if(!is_array($args)){//convert to array
			$args=array($args);
		}
		try{
			if(!is_null($object)){
				//if(!ENABLE_DEBUG_METHODS){
				//	\shopping_agg\error_handle\error_log_c("Error: please ENABLE_DEBUG_METHODS to true if you are going to be debugging api objects :)");
				//	return false;
				//}
				//$object_s=serialize($object);//turn object into string
				//$response=eval(" unserialize('$object_s')->debug_test('$function',".implode(",",array_map(function($arg){return var_export($arg,true);},$args)).");") ;
				$rm=new \ReflectionMethod((is_string($object)?$object:get_class($object))."::".$function);
				$rm->setAccessible(true);
				$response=$rm->invokeArgs($object,$args);
			}
			else{
				$response=call_user_func_array($function,$args);//eval("return $function(".implode(",",array_map(function($arg){return var_export($arg,true);},$args)).");");
			}
		}
		catch(\Exception $e){
			\shopping_agg\error_handle\error_log_c("Error: debbugging failed for call to $function\n (Error thrown with a message of". $e->getMessage(),@var_export(debug_backtrace(),true) );//log all errors from debugging
			return false;
		}
		debug_print("Made ".((is_null($object)?"function call to $function(":"method call to ".get_class($object)."::$function(").implode(",",array_map(function($arg){return @var_export($arg,true);},$args))).")<br/>that returned:<br/><div style='background-color:#b3b3cc'> ".@var_export($response,true)."</div><br/>".(($expected_outcome===$response)?" <b style='color:green'>successfully ":" <b style='color:red'>not ")."</b>matching the expected result of: <div style='background-color:#b3b3cc'>".@var_export($expected_outcome,true)."</div><hr/>");//Heyyyyy thats a pretty guud line of code (nastiest line of code i've ever done)
		$max_csv_len=50;//the max csv export length
		return str_ireplace("\n", " ", //remove new line charicters
				implode(",",array_map(function($data){
					return str_ireplace(",","�",$data);//Please not that the comma to the left is not the same utf8 charicter as the one to the right :)
				}
						, array(//use "," as a deliminator
					(($expected_outcome===$response)?"TRUE":"FALSE"),//if it was true or false
					((is_null($object))?"$function":get_class($object)."::$function"),//The method call
					strlen(@var_export($args,true))>$max_csv_len+1?substr(@var_export($args,true),0,$max_csv_len)."...":@var_export($args,true),//The passed arguments
					strlen(@var_export($expected_outcome,true))>$max_csv_len+1?substr(@var_export($expected_outcome,true),0,$max_csv_len)."...":@var_export($expected_outcome,true),//the expected outcome
					strlen(@var_export($response,true))>$max_csv_len+1?substr(@var_export($response,true),0,$max_csv_len)."...":@var_export($response,true)//the real outcome
		))));
	}
}
/**
 * Makes a batch call to debug test.
 * @param array $args The arguments to pass to the debug test in an 2 dimentional array of calls for the argument.
 * @param string $debug_file The directory to place the returned debug data file into.
 * @return string|null The debug csv string.
 */
function debug_batch_test($args=array(),$debug_file="./"){
	if(!DEBUGGING){
		return null;
	}
	if(!is_array($args)){
		\shopping_agg\error_handle\error_log_c("Error: invalid argument passed to debug batch test (expecting array) ".gettype($var)."passed (skipping this call)", $args);
	}
	$final_data="'Successfull','called method/function','passed arguments','expected outcome','real outcome'\n";
	foreach ($args as $arg){
		if(!is_array($arg)){//if the argument is not an array
			\shopping_agg\error_handle\error_log_c("Error: while making a batch call to debug test an set of arguments was passed a ".gettype($arg)." and not an array.(skipping this call)",$arg);//throw error
			continue;//contiue to next iteration.
		}
		if(!in_array(count($arg), array(3,4))){
			\shopping_agg\error_handle\error_log_c("Error: Insufficient number of arguments passed.(or too many) Expecting 3 or 4 got".(string)count($arg),$arg);//throw error
			continue;
		}
		
		$response=debug_test($arg[0],$arg[1],(isset($arg[2]))?$arg[2]:NULL,(isset($arg[3]))?$arg[3]:NULL);//call debug test function
		if(!$response){
			\shopping_agg\error_handle\error_log_c("Error: Unable to properly make call to debug_test due to inturnal function error.(skipping this call)", $arg);
			continue;//Skip iteration due to invalid response
		}
		$final_data.=$response."\n";
	}
	file_put_contents($debug_file."debug_data.csv",$final_data);//save file contents at defined file location
	return $final_data;//return csv data
}/*
if(ENABLE_DEBUG_METHODS){//only define the trait if debugging methods are enabled
	/**
	 * 
	 * @author owner
	 * A trait for debugging classes with private methods so that methods can be invoked from an inturnal scope
	 *
	trait private_debug_call{
	
		/**
		 * This is a method for inurnal debugging
		 * @param string $method The method name
		 * @param unknown ...$args the arguments to pass to the method
		 * @return mixed the result of the debugging
		 *
		public function debug_test($method,...$args){
			
			
			//return eval("return \$this->$method(".implode(",",array_map(function($args){return var_export($args,true);},$args)).");");
		}
	}
}
else{
	/**
	 * @ignore
	 *
	trait private_debug_call{
		public function debug_test($method,...$args){return null;}
	};//define trait with on possible 
}*/
?>