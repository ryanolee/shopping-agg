<?php
/**
 * This document defines the main error hanling functions for the shopping aggregator module.
 * @author rizza <rizza@rizza.net>
 * @package shopping_aggregator\functions\error_hanling
 */
/**
 * The error_handle namespace is defined for all error handeling functions and classes of the shopping aggregator module.
 */
namespace shopping_agg\error_handle;
/**
 * This function takes an erro code and returns its type
 * @param unknown $type
 * @return string
 */
function FriendlyErrorType($type)
{
	switch($type)
	{
		case E_ERROR: // 1 //
			return 'E_ERROR';
		case E_WARNING: // 2 //
			return 'E_WARNING';
		case E_PARSE: // 4 //
			return 'E_PARSE';
		case E_NOTICE: // 8 //
			return 'E_NOTICE';
		case E_CORE_ERROR: // 16 //
			return 'E_CORE_ERROR';
		case E_CORE_WARNING: // 32 //
			return 'E_CORE_WARNING';
		case E_COMPILE_ERROR: // 64 //
			return 'E_COMPILE_ERROR';
		case E_COMPILE_WARNING: // 128 //
			return 'E_COMPILE_WARNING';
		case E_USER_ERROR: // 256 //
			return 'E_USER_ERROR';
		case E_USER_WARNING: // 512 //
			return 'E_USER_WARNING';
		case E_USER_NOTICE: // 1024 //
			return 'E_USER_NOTICE';
		case E_STRICT: // 2048 //
			return 'E_STRICT';
		case E_RECOVERABLE_ERROR: // 4096 //
			return 'E_RECOVERABLE_ERROR';
		case E_DEPRECATED: // 8192 //
			return 'E_DEPRECATED';
		case E_USER_DEPRECATED: // 16384 //
			return 'E_USER_DEPRECATED';
	}
	return "UNKNOWN_ERROR_TYPE";
}
/**
 * This is a custom alias of error_log ,hence error_log_c,that contains custom functionality based of the values of the LOGGING constants
 * @param string $message The message of the error to log.
 * @param mixed[] ...$problem_vars The related variables that may be causing the errors. (Strings will be logged directly)
 */
function error_log_c($message,...$problem_vars){//define function called error_log_c
	if (DEBUGGING){//if debugging is enabled
		\shopping_agg\debug\debug_print("Error logged as:'<i>".$message."'</i>", $problem_vars);//print the debug message
	}
	if(LOGGING){//if logging is enabled..
		$msg_final="";// define final message to log
		if(LOGGING_ERRORS&&!LOGGING_ALL){//and only logging errors is set..
			preg_match("/^\\s*error/i",$message,$error);//check message for if it an error(By using regex to case insensitivly chaec thet the string starts with error)
			if(sizeof($error)!=0){//be looking at front of the message and see if it is "Error:... bla bla bla" 
			    $msg_final.=$message;//log that error
			}
		}
		if(LOGGING_WARNINGS&&!LOGGING_ALL){//Look for if logging of warnings is set exclusive of if logging of all errors.
			preg_match("/^\\s*warning/i",$message,$warnings);//do the same thing from the error segment but with "Warning:... bla bla bla"
			if(sizeof($warnings)!=0){//See that the regex found something
				$msg_final.=$message;//log the warning
			}
		}
		if(LOGGING_ALL){//if logging all is set (The two if statements above would have not triggered)
			$msg_final.=$message;//log the passed string
		}
		if(LOGGING_VARS){//if var logging has been enabled
			if(sizeof($problem_vars)==0){//if there are no given problem variables
				$msg_final.="\nNo value passed.";//say that there has been no data given to dump
			}
			foreach($problem_vars as $problem){//otherwise iterate through the problem varibles
				if(is_string($problem)){//if it is a string ...
					$msg_final.=$problem;//log it directly
				}
				else{//oterwise...
					$msg_final.="Type:".gettype($problem_vars)."\nValue:".@var_export($problem,true);//log formatted error
				}
			}
		}
		//debug_backtrace_print();//(if yau are still having troubles with this uncoment this line )
		error_log($msg_final);//log the error in  the end 
	}
	
}
//@todo set up error handlers for all errors
/**
 * This function disables human readable errors while haveing all errors/warnings/notices trigger for errors logging.
 */
function disable_errors(){
	$GLOBALS["error_handlers"]["prev_level"]=error_reporting();//save previous level
	$GLOBALS["error_handlers"]["prev_level_ini"]=ini_get('display_errors');//save previous ini level
	error_reporting(0);//try to block error reporting
	ini_set('display_errors', 'Off');//set the php ini to not report errors incase the function aboe fails to do so.
}
/**
 * This is the custom callback to help log unhandled errors as part of  @link<http://php.net/manual/en/function.set-error-handler.php>
 * @param int $error_number The first parameter, errno, contains the level of the error raised, as an integer.
 * @param string $error_string The second parameter, errstr, contains the error message, as a string.
 * @param string $err_file The third parameter is optional, errfile, which contains the filename that the error was raised in, as a string.
 * @param int $err_line The fourth parameter is optional, errline, which contains the line number the error was raised at, as an integer.
 * @param array $err_context The fifth parameter is optional, errcontext, which is an array that points to the active symbol table at the point the error occurred. In other words, errcontext will contain an array of every variable that existed in the scope the error was triggered in. User error handler must not modify error context.
 * @return boolean returns true to block default error handle.
 */
function unhandled_error_error_handle($error_number,$error_string,$err_file,$err_line,$err_context){
	error_log_c("Error: Unexpected ".FriendlyErrorType($error_number)." thrown in $err_file on line ".(string)$err_line." with the message '$error_string'.", $err_context);
	return true;//ignore php(s) inturnal error handler
}

/**
 * This function begins error logging and blocking of all error mesages to the end user
 */
function begin_error_handle(){
	if(!DEBUGGING){//if debugging is disabled
		set_error_handler("\\shopping_agg\\error_handle\\unhandled_error_error_handle",E_ALL);
		disable_errors();//block default errors
	}
}
/**
 * This function ends error handling
 */
function end_error_handle(){
	if(!DEBUGGING){//if debugging is disabled
		restore_error_handler();//revert error handler to default config if there was one
		enable_errors();//block default errors
	}
}
/**
 * Restores previous values after disable_errors has been called. PLEASE NOTE THAT CALLING THIS BEFORE disable_errors will result in a fatal error.
 * @see \shopping_agg\error_handle\disable_errors()
 */
function enable_errors(){
	error_reporting($GLOBALS["error_handlers"]["prev_level"]);
	ini_set('display_errors',$GLOBALS["error_handlers"]["prev_level_ini"]);
}
/**
 * 
 */
function send_user_error(){

}

?>