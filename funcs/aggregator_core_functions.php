<?php
/**
 * This file defines the main function to be called for each api as defined in the ALLOWED_API_FILES(definned in ./constants/config.php) constant using 
 * func_name=>file pairings (Each function must take one argument bieng the api object passed to the function and return the data the api should yield in  that given context or false on error)
 */
//DO NOT USE session_start in any of these functions.
//they use concurrent execution so trying to use sessin start on 2 concurrent executions will result in an error (so please dont do it :) )
namespace shopping_agg\agg_main_funcs;
/**
 * The main aggragator functions for the amazon api
 * @param \shopping_agg\api\api $api
 */
function amazon($api){
	$d1=$api->search->exec_request();
	$d2=array_merge_recursive($d1,$api->get_next_data());
	//session_start();
	return $d2;
	
}
function amazon2($api) {
	//session_start();
	return $api->search->exec_request();
}
function shop_api($api){
	return $api->search->exec_request();
}