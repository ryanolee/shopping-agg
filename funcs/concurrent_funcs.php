<?php
/**
 * This document defines functions that are to be used during the execution of concurrent api calls
 */
namespace shopping_agg\concurrent;
use function shopping_agg\debug\debug_print;
use function shopping_agg\error_handle\error_log_c;

function concurrent_api_call(){
	$apis=unserialize(ALLOWED_API_FILES);//get allowed api files
	$apis=array_chunk($apis, MAX_API_CONCURRENT_PROCESSES, true);//cunk array into it's componant parts
	$url= (isset($_SERVER['HTTPS'])?"https://":"http://").getHostByName(getHostName()).dirname(ltrim($_SERVER["PHP_SELF"],"funcs"))."/cuncurrent_calls/concurrent_handle.php";//Get the request 
	debug_print("Request to inturnal url $url has been made for concurrent execution");
	$concurrent_state=serialize(array("get"=>$_GET,"post"=>$_POST));
	$response=array_map(function($api_block)use($url,$concurrent_state){// for each api block
		$keys=array_keys($api_block);
		$token=sha1(mt_rand());
		file_put_contents(__DIR__."/../cuncurrent_calls/tokens/".$token.".txt", (string)(time()+10));//leave token valid for 10 seconds
		$connections=array_map(function($key)use($url,$concurrent_state,$token){
			$ch=curl_init($url);//init the url
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);//have results be returned
			curl_setopt($ch, CURLOPT_POST, true);//make it a post request
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array("concurrent_state"=>$concurrent_state,"data_to_get"=>$key,"token"=>$token)));//build the http request
			curl_setopt($ch,CURLOPT_TIMEOUT,MAX_API_EXECUTION_TIME);
			return $ch;
		},$keys);
		
		$multi_handle=curl_multi_init();//create the multi handle (to enable concurrency)
		foreach($connections as $ch){
			curl_multi_add_handle($multi_handle,$ch);//bind the multi handles
		}
		$running=null;
		$session_to_start=false;
		if(session_status()==PHP_SESSION_ACTIVE){//if we are running a session
			session_write_close();//close it so we dont lock the session file and break our php;
			$session_to_start=true;
		}
		do {
			$result=curl_multi_exec($multi_handle, $running);//execute the multihandle
		} while ($running);
		if($session_to_start){
			session_start();//persist session
		}
		foreach($connections as $ch){
			curl_multi_remove_handle($multi_handle,$ch);//unbind the multi handles
		}
		curl_multi_close($multi_handle);//close the multihandle
		$response=array();
		$iter=new \MultipleIterator();//init new itter
		$iter->attachIterator(new \ArrayIterator($keys),"key");//bind keys
		$iter->attachIterator(new \ArrayIterator($connections),"connection");//bind connections
		foreach ($iter as $i){
			
			$response[$i[0]]=array(curl_multi_getcontent($i[1]),$i[1]);//[0] response data [1] connection response
		}
		array_walk($response, function(&$response_data,$key){
			$ch=$response_data[1];
			$response=$response_data[0];
			$http_status=intval(curl_getinfo($ch, CURLINFO_HTTP_CODE));//look at the status code
			if($http_status!==200 ){
				$codes = unserialize(HTTP_STATUS_CODES);
				$code_msg=(isset($codes[$http_status]))?$codes[$http_status]:"Unknown status code.";//if there is an error code set get the msg (otehrwise get nothing);
				\shopping_agg\error_handle\error_log_c("Error: inturnal server request failed to [$key]: returned status code ".(string)$http_status.": $code_msg");
				return null;
			}
			if(strlen($curl_error_msg = curl_error($ch)) !==0 ){
				\shopping_agg\error_handle\error_log_c("Error: inturnal server request failed to [$key]: inturnal curl error $error");
				return null;
			}
			$response_new=@unserialize($response);
			if(!$response_new||!isset($response_new["content"])||!isset($response_new["valid"])){
				\shopping_agg\error_handle\error_log_c("Error: Invalid response from inturnal server call for [$key]. (malformed response data)");
				\shopping_agg\debug\debug_iframe_print("<h1>Glitched response is as follows:</h1>".$response);
				return null;
			}
			$response=$response_new;
			if(isset($response["debug"])&&DEBUGGING){
				\shopping_agg\debug\debug_print("Request finished in: ".(isset($response["time"])?$response["time"]:"unknown")." seconds.<br/>Debug information from api [$key] is as follows:");
				\shopping_agg\debug\debug_iframe_print($response["debug"]);
			}
			if(!$response["valid"]){
				\shopping_agg\error_handle\error_log_c("Error: response not valid for [$key]. Error registered from request ".$response["content"]);
				return null;
			}
			
			$response_data=unserialize($response["content"]);
		});
		unlink(__DIR__."/../cuncurrent_calls/tokens/".$token.".txt");//delete file
		return $response;
		
		
	}, $apis);
	$final=array();
	foreach ($response as $chunk){//flatten array
		$final=array_replace($final, $chunk);
	}
	foreach (glob(__DIR__."/../cuncurrent_calls/tokens/*.txt") as $file){//link files
		if((integer)file_get_contents($file)<time()){
			unlink($file);
		}
	}
	return $final;
}
