<?php
$t1=microtime(true);

/**
 * This document is for inturnal concurrent aggragator execution and acts as its own special page for getting results from a singly specified api and retuning them in a serelized form.
 */
if(!isset($_POST["token"])){
	echo serialize(array("valid"=>false,"content"=>"Error: token not passed"));//return error
	exit;//terminate script
}
if(!file_exists(__DIR__."/tokens/".$_POST["token"].".txt")){
	echo serialize(array("valid"=>false,"content"=>"Error: invalid token"));//return error
	exit;//terminate script
}
$token_time=file_get_contents(__DIR__."/tokens/".$_POST["token"].".txt");
if((integer)$token_time<time()){
	echo serialize(array("valid"=>false,"content"=>"Error: invalid token 2 $token_time >".(string)time()));//return error
	exit;//terminate script
}

// This script is inplace to allow for concurrent aggragator execution 
//THIS SCRIPT SHOULD NEVER BE ON AN OWTWARD FACING ACCESS;
//ALLWAYS BLOCK WITH .htaccess!!!
if(!isset($_POST["concurrent_state"])){//if our concurrent state is not set throw an error
	echo serialize(array("valid"=>false,"content"=>"Error: required POST field ['concurrent_state'] not set"));//return error
	exit;//terminate script
}
$_POST["concurrent_state"]=@unserialize($_POST["concurrent_state"]);//get our concurrrent state
if(!$_POST["concurrent_state"]||!isset($_POST["concurrent_state"]["get"])||!isset($_POST["concurrent_state"]["post"])){//if oure concurrent state object is invalid
	echo serialize(array("valid"=>false,"content"=>"Error: malformed POST field ['concurrent_state']"));//throw an error
	exit;
}
//get our constants
require_once __DIR__.'/../constants/config.php';//get config
require_once __DIR__.'/../constants/invalid_types.php';//get invalid types
require_once __DIR__.'/../constants/defaults.php';//get default values
if(!in_array(isset($_POST["data_to_get"])?$_POST["data_to_get"]:"undefined", array_keys(unserialize(ALLOWED_API_FILES)))){
	echo serialize(array("valid"=>false,"content"=>"Error: Invalid api passed to interact with"));//throw an error
}
$_GET=array_replace($_GET,$_POST["concurrent_state"]["get"]);//throw an error
$_POST=array_replace($_POST,$_POST["concurrent_state"]["post"]);//throw an error


//BRING IN DEPENDINCIES START
require_once __DIR__.'/../exturnal_deps/phpquerey/php_querey_core.php';
require_once __DIR__.'/../funcs/error_handle.php';//define error handles (mostley error logging)
require_once __DIR__.'/../funcs/parse.php';//define parse
require_once __DIR__.'/../funcs/debug.php';
require_once __DIR__.'/../funcs/extra.php';
//GET API CLASSES
require_once __DIR__.'/../classes/abstract.php';
require_once __DIR__.'/../classes/api_keys.php';
require_once __DIR__.'/../classes/api_key.php';
require_once __DIR__.'/../classes/api.php';
//GET INTERACTIONAL CLASSES
require_once __DIR__.'/../classes/interaction/interaction.php';
require_once __DIR__.'/../classes/interaction/endpoint.php';
require_once __DIR__.'/../classes/merge/merge.php';
require_once __DIR__.'/../classes/interaction/response.php';
$file_to_get=unserialize(ALLOWED_API_FILES)[$_POST["data_to_get"]];
$cahced_file=__DIR__."/../serelized_aggragator_componants/".str_replace(".json",".bin", $file_to_get);
$json_file=__DIR__."/../json/".$file_to_get;

/**
 * This function is used when the api needs to ba parsed (may result in script termination)
 * @param  $json_file the directory of the file to build the api object from
 * @param bool $force_overwright If the cache file should be ovewitten forcibly
 * @return \shopping_agg\api\api
 */
function build_api($json_file,$force_overwright=false){
	if(!file_exists($json_file)){
		echo serialize(array("valid"=>false,"content"=>"Error: File $file_to_get does not exsist. Please check the ALLOWED_API_FILES constant."));//throw an error
		exit;
	}
	if(ob_get_level()===0){
		ob_start();//start buffering output
	}
	$api_object=\shopping_agg\parse\json_to_interactional_layer(file_get_contents($json_file));//build our api file
	if(!$api_object){//if parsing failed
		echo serialize(array("valid"=>false,"content"=>ob_get_clean()));//throw an error
		while(ob_get_level()!==0){
			@ob_end_flush();//terminate output buffering
		}
		exit;
	} 
	if(CACHING_ENABLED){//ig api caching is enabled
		$file_to_get=unserialize(ALLOWED_API_FILES)[$_POST["data_to_get"]];
		$cahced_file=__DIR__."/../serelized_aggragator_componants/".str_replace(".json",".bin", $file_to_get);
		if(!file_exists(dirname($cahced_file))){//if the file does not exsist
			mkdir(dirname($cahced_file));
		}
		if(!file_exists($cahced_file)||$force_overwright){//serelize the file and store it if it is in need of update
			file_put_contents($cahced_file, serialize(array("sha1"=>sha1_file($json_file),"api"=>$api_object)));
		}
	}
	return $api_object;
}
ob_start();//begin output buffering
if(!file_exists($cahced_file)||!CACHING_ENABLED){//if our file is not cached
	$api=build_api($json_file);
}
else{
	//try{
	$api=@unserialize(file_get_contents($cahced_file));//get cached file
	if(!$api){
		echo serialize(array("valid"=>false,"content"=>"Failed to parse api cache ($cahced_file)"));
		exit;
	}
	if(!isset($api["sha1"])||!$api["api"]){
		echo serialize(array("valid"=>false,"content"=>"Malformed api cache (Please delete $cahced_file) "));
		exit;
	}
	if(sha1_file($json_file)!==$api["sha1"]||DEBUGGING){
		$api=build_api($json_file,true);
	}
	else{
		$api=$api["api"];//set api to it's proper value
	}
	//}
}
require_once __DIR__.'/../funcs/aggregator_core_functions.php';
if(DEBUGGING){
	ini_set('xdebug.var_display_max_depth', 50);//allow massive var_dumps in debug mode
	ini_set('xdebug.var_display_max_children', 256);
	ini_set('xdebug.var_display_max_data', 1024);
}

if(!function_exists("shopping_agg\\agg_main_funcs\\".$_POST["data_to_get"])){
	echo serialize(array("valid"=>false,"content"=>"Error: function shopping_agg\\agg_main_funcs\\".$_POST["data_to_get"]." does not exsist where it should."));
	exit;
}
//ob_start();
$response=call_user_func("shopping_agg\\agg_main_funcs\\".$_POST["data_to_get"],$api);
if(!$response){
	echo serialize(array("valid"=>false,"debug"=>ob_get_clean(),"content"=>"Error: something when wrong while calling the core aggregator function.(shopping_agg\\agg_main_funcs\\".$_POST["data_to_get"].")"));//throw an error
	while(ob_get_level()!==0){
		@ob_end_flush();//terminate output buffering
	}
	exit;
}
else{
	echo serialize(array("valid"=>true,"debug"=>ob_get_clean(),"content"=>serialize($response),"time"=>(microtime(true)-$t1)));//throw an error
	while(ob_get_level()!==0){
		@ob_end_flush();//terminate output buffering
	}
	exit;
}


