<?php
//doc block broken in eclips for unknown reason
/**
 * 	This document defines all the configuration constants for the interactional layer of the php.
 *  @author rizza<rizza@rizza.net>
 *  @package shopping_aggregator\constants\configuration
 */


namespace shopping_agg;
use api\interactinal_class;
//DEBUGGUING
/**
 * The dubugging constant controls if the script sees itself to be in an debugging state. <br/>If true debugging functionality will be added to the code.<br/> If false debugging funcionality will be used.<br/> Please set this to true if you are adding a new website to the aggregator.
 * @var bool
 * @see debug\debug_print 
 */
define("DEBUGGING",true);
/**
 * This constant defines if data that has been returned from API(s) should be printed to the screen during debugging. 
 * Set to true while making json configuration files for new API(s) 
 * @var bool
 */
define("DEBUG_SHOW_DATA",false);
/**
* Theis constant controlls if related variables should be mapped bound to debug error printouts
* @var bool
* @see debug\debug_print
*/
define("DEEP_DEBUGGING",false);
/**
 * This constant defines if raw data should be dumped from api requests. 
 * @var bool
 */
define("DUMP_DATA_FROM_REQUESTS",false);
/**
 * @ignore
 * The enable_test_methods constant adds extra methods to classes  that allows for fast testing of methods and funtions. <h1><b><u>YOU SHOULD ONLY ENABLE THIS ON OFFLINE BUILDS</u></b></h1>
 * @var bool
 */
//define("ENABLE_DEBUG_METHODS",true);
//ERROR HANDLING
/**
 * The logging constant defines wether errors should be logged to the default error logging managment method.(as defined in the php.ini)
 * @var bool
 * @see \shopping_agg\error_handle\error_log_c
 */
define("LOGGING",1);//define constant
ini_set("log_errors", LOGGING);
/**
 * This constant defines the file location for where errors should be logged to
 * @var string
 */
define("LOGGING_FILE","/agg_log.log");
ini_set("error_log", LOGGING_FILE);
/**
 * wether to log errors or not (This is not relavent if LOGGING_ALL is enabled)
 * @var bool
 * @see LOGGING_ALL
 * @see \shopping_agg\error_handle\error_log_c
 */
define("LOGGING_ERRORS",true);
/**
 * Wether to log warnings or not (This is not relavent if LOGGING_ALL is enabled)
 * @var bool
 * @see \shopping_agg\error_handle\error_log_c
 */
define("LOGGING_WARNINGS",true);
/**
 * Log all messages sent to \shopping_agg\error_handle\error_log_c (This includes errors warnings and any other sets sent to the log)
 * @var bool
 * @see \shopping_agg\error_handle\error_log_c
 */
define("LOGGING_ALL",true);
/**
 * the logging vars variable is resposible for if the value of the variable causing issues is looged alongside the error it causes. <br/>(This may result in some of logs being unexpectedly long as each variable is dumed to the log)
 * @var bool
 * @see \shopping_agg\error_handle\error_log_c
 */
define("LOGGING_VARS",true);
/**
 * Change this value to the desired max length(in bytes) of each log entry.( given var_export($var,true) is used alot to get the true value of an error. during logs)
 * default 10000
 * @var int
 * @see http://www.php.net/manual/en/errorfunc.configuration.php
 */
define("MAX_LOG_ENTRY_LENGTH",10000);//define constant
ini_set("log_errors_max_len", MAX_LOG_ENTRY_LENGTH);//apply constant
//NEURAL NET FUNCTIONALITY
/**
 * This variable defines if training methods should be included and executed. This will result in the main script not being called. This should only be set to true if the neuralnetwork governing item merging is in need of updating.
 * @var bool
 */
define("NEURAL_NETWORK_TRAINING",false);
//CLASS FUNCTIONALITY
/**
 * allow falldown api behavior for methods of the api classes.<br/>
 * (This means that if a method called on one instance of its class does not exsist then it will look on its master for the method then the masters master.. ect up untill the root node where it will then throw a fatal error and exit.)
 * @see api\interactinal_class::__call
 * @see api\interactinal_class::handle_invisable_method_call
 * @var bool
 */
define("ALLOW_OVERFLOW_METHODS",false);//define constant
//MERGE FUNCTIONALITY
/**
 * This constant defines if extra urls gone to during the merge phase should be flattended into a single layer or there structure should remain constant
 * @var bool
 */
define("FLATTEN_MERGE_STRUCTURE",false);
/**
 * Selectors in the merge layer of the interactional api object must yeild the same number of items.
 * @var bool
 */
define("STRICT_ITEM_COUNT",false);
/**
 * This constant defines if the data should be normalized or into a regular format or not. The codes are as follows:
 * 0->Dont organise
 * 11->normalize strictly into an associative array(where sctrictly is where the script will failure with irregular item lengths)(recoursivly)
 * 12->normalize strictly into a stdObject strictly(recoursivly)
 * 21->normalize into an associative array non-strictly(recoursivly)
 * 22->normalize into a stdObject(non_strictly)
 * @var int
 */
define("NORMALIZE_API_OBJECTS",0);
/**
 * This variable defines the acceptable parsable json files to inhibit any code injection for concurrent aggregator execution $_POST[$data]=>$file pairings
 */
define("ALLOWED_API_FILES",serialize(array(
		"amazon"=>"amazon2.json",
		//"amazon2"=>"amazon2.json"
		"shop_api"=>"shopping.json"
)));
/**
 * This uses serelized object files to increase the speed of api interaction instead of parsing json from scratch. 
 * @var bool
 */
define("CACHING_ENABLED",true);
/**
 * This constant defines the maximum number of api(s) that can be contacted at once to ensure that the does not send too many requests to itself (each request is equal to a new webpage bieng loaded)
 * @var unknown
 */
define("MAX_API_CONCURRENT_PROCESSES",2);
/**
 * The maximum amount of time any given set of interactions should be allowed to go on for
 * @var int
 */
define("MAX_API_EXECUTION_TIME",120);

?>