<?php
/**
 * This document defines all the fallback values for if a passed variable fails its type check
 * @author rizza <rizza@rizza.net>
 * @package shopping_aggregator\constants\invalid_types
 */
namespace shopping_agg;
/**
 * The default value for an invalid url (as per validation for the interactional construct)
 * @var mixed
 */
define("INVALID_URL",false);
/**
 * The default value for a request in the eventuality that an invalid method for an api endpoint object
 * @var string
 */
define("INVALID_REQUEST_METHOD","GET");