<?php
/**
 * This document defines the default/fallback values for where options are omitted , non-exsistant or invalid.
 * @author rizza <rizza@rizza.net>
 * @package shopping_aggregator\constants\default_values
 */
namespace shopping_agg;
/**
 * <u><b>THIS MUST BE UNSERILIZED BEFORE USE!</b></u>
 * This constant holds all the default values for curl requests
 * @var array
 */
define("API_CURL_DEFAULTS",serialize(array(//serilize for pre php 5.6 souppourt (see: http://stackoverflow.com/questions/1290318/php-constants-containing-arrays)
		"method"=>"GET",
		"api_key"=>"DEFAULT",//DEFAULT refrences the default api key (So if you thought it would be a good idea to have your api key value as "DEFAULT" it was not >:) ) 
		"data"=>[],
		"post_data"=>[],
		"get_data"=>[]
)));
/**
 * <u><b><H1>THIS MUST BE UNSERILIZED BEFORE USE!</h1></b></u>
 * This constant holds the required key values for the core API curl method 
 * @var array
 */
define("API_CURL_REQUIREMENTS",serialize(array(
		"endpoint"
)));
/**
 * <u><b><H1>THIS MUST BE UNSERILIZED BEFORE USE!</h1></b></u>
 * This constant holds the default curl options (to be passed into the curl_setopt_array).
 * @var array
 * @see curl_setopt_array
 */
define("CURL_DEFAULTS",serialize(array(
		CURLOPT_SSL_VERIFYPEER=>false,//ignore ssl
		CURLOPT_FOLLOWLOCATION=>true,//follow redirects
		CURLOPT_MAXREDIRS=>5,//in a redirect chain only allow 5
		CURLOPT_NOPROGRESS=>!DEBUGGING,//if debugging show progress (double inversin ie if debugging is true don't don't show the progress. hows the not not questions going for you :) )
		CURLOPT_VERBOSE=>DEBUGGING//be verbose if debugging
		
)));
/**
 * The default value for response keys to look for array items
 * @var string
 */
define("DEFAULT_NEXT_URL_IDENTIFIER","!!!NEXT!!!");
/**
 * A serilized array of all the absolutly required variables for the aggragator to get form an api. Errors will be thrown if thease are not present in the vars_to_look_for response.
 * @var array
 */
define("VARS_TO_LOOK_FOR_REQUIRED", serialize(array(
		//"name"=>"string",
		//"description"=>"string",
		//"price"=>"float",
		//"image_url"=>"string"
)));
/**
 * This constant defines a serilized array of http status codes in the form [int $codeid]=>code_info
 */
define("HTTP_STATUS_CODES",serialize( array(
    100 => 'Continue',
    101 => 'Switching Protocols',
    102 => 'Processing',
    200 => 'OK',
    201 => 'Created',
    202 => 'Accepted',
    203 => 'Non-Authoritative Information',
    204 => 'No Content',
    205 => 'Reset Content',
    206 => 'Partial Content',
    207 => 'Multi-Status',
    300 => 'Multiple Choices',
    301 => 'Moved Permanently',
    302 => 'Found',
    303 => 'See Other',
    304 => 'Not Modified',
    305 => 'Use Proxy',
    306 => 'Switch Proxy',
    307 => 'Temporary Redirect',
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    406 => 'Not Acceptable',
    407 => 'Proxy Authentication Required',
    408 => 'Request Timeout',
    409 => 'Conflict',
    410 => 'Gone',
    411 => 'Length Required',
    412 => 'Precondition Failed',
    413 => 'Request Entity Too Large',
    414 => 'Request-URI Too Long',
    415 => 'Unsupported Media Type',
    416 => 'Requested Range Not Satisfiable',
    417 => 'Expectation Failed',
    418 => 'I\'m a teapot',
    422 => 'Unprocessable Entity',
    423 => 'Locked',
    424 => 'Failed Dependency',
    425 => 'Unordered Collection',
    426 => 'Upgrade Required',
    449 => 'Retry With',
    450 => 'Blocked by Windows Parental Controls',
    500 => 'Internal Server Error',
    501 => 'Not Implemented',
    502 => 'Bad Gateway',
    503 => 'Service Unavailable',
    504 => 'Gateway Timeout',
    505 => 'HTTP Version Not Supported',
    506 => 'Variant Also Negotiates',
    507 => 'Insufficient Storage',
    509 => 'Bandwidth Limit Exceeded',
    510 => 'Not Extended'
)));
/**
 * This contains allowed special methods that can be called irrispective of object chaining or not
 * MUST BE unserialized before use
 * @var array
 */
define("ALLOWED_OVERFLOW_METHODS",serialize(array( 
		"get_root_node"
)));