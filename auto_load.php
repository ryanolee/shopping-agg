<?php
/**
 * This document autoloads all required dependincies and related methods,functions and classes for this modules script execution
 * @author rizza <rizza@rizza.net>
 */

namespace shopping_agg;


$_POST["long"]="200";
$_GET["key"]=isset($_GET["key"])?$_GET["key"]:"test";
//require_once './exturnal_deps/phpdoc.phar';exit();
//exit();

if(!in_array  ('curl', get_loaded_extensions())){
	throw new \Exception("Error: it is critical for cURL be enabled for this php module to work please enable cURL before importing.");
}
else{
	autoload_api();
}
function autoload_api(){
	
	
	$prev_dir=getcwd();//save current working directory as a pointer so that the php scripts may properly reset after autoload execution.
	chdir(dirname(__FILE__));//Change current working directory to this file to allow for relitavistic includes.
	//GET EXTURNAL DEPS
	require_once './exturnal_deps/neural_net/neural_net.php';
	require_once './exturnal_deps/phpquerey/php_querey_core.php';
	require_once './exturnal_deps/thread/thread.php';
	//BRING IN DEPENDINCIES START
	require_once './constants/config.php';//get config
	require_once './constants/invalid_types.php';//get invalid types
	require_once './constants/defaults.php';//get default values
	require_once './funcs/error_handle.php';//define error handles (mostley error logging)
	require_once './funcs/parse.php';//define parse
	require_once './funcs/debug.php';
	require_once './funcs/extra.php';
	require_once './funcs/concurrent_funcs.php';
	//GET API CLASSES
	require_once './classes/abstract.php';
	require_once './classes/api_keys.php';
	require_once './classes/api_key.php';
	require_once './classes/api.php';
	//GET INTERACTIONAL CLASSES
	require_once './classes/interaction/interaction.php';
	require_once './classes/interaction/endpoint.php';
	require_once './classes/merge/merge.php';
	require_once './classes/interaction/response.php';
	
	//GET CONDITIONAL FILES
	if(NEURAL_NETWORK_TRAINING){
		require_once './funcs/neural_net_training.php';
		
	}
	else{
		//BRING IN DEPENDENCIES END
		if(DEBUGGING){
			ini_set('xdebug.var_display_max_depth', 50);//allow massive var_dumps in debug mode
			ini_set('xdebug.var_display_max_children', 256);
			ini_set('xdebug.var_display_max_data', 1024);
		}
		require_once './funcs/main.php';//get the main initilization code;
		\shopping_agg\error_handle\begin_error_handle();//begin error handling
		\shopping_agg\main\main();
		\shopping_agg\error_handle\end_error_handle();//begin error handling
		chdir($prev_dir);//reset cwd
		unset($prev_dir);//clean up rubbish
	}
	
};