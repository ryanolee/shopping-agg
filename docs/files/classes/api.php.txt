<?php
/**
 * Defines the core api class for aggregator interactions
 * @author rizza <rizza@rizza.net>
 * @package shopping_aggregator\interactional_layer\core
 */

namespace shopping_agg\api;//define that this function is part of the api namespace
use \shopping_agg\interaction\interaction as interaction;
use \shopping_agg\interaction\endpoint as endpoint;
use \shopping_agg\interaction\response as response;
use function shopping_agg\error_handle\error_log_c;
use shopping_agg;


/**
 * This is the core class for the intaractional layer and contains all information neccacery for interactions with an api.
 * @author rizza <rizza@rizza.net>
 * @property string $name The name of the api
 * @property string $image_url The full url link to image for the logo of the api
 * @property \shopping_agg\interaction\interaction $search The main search object
 * @property bool $requires_authkey If this api is open of requires an authentication key
 * @property unknown $master The master object to the api root node (Yet to be implamened)
 *
 */
class api extends interactinal_class{
	/**
	* This is the constructor method for the api class
	* @param string $name The name of the api
	* @param string $image_url The full url link to image for the logo of the api
	* @param \shopping_agg\interaction\interaction $search The main search object
	* @param bool $requires_authkey If this api is open of requires an authentication key
	* @param unknown $master The master object to the api root node (Yet to be implamened)
	* @param \shopping_agg\api\api_keys The api keys object to be used. (only required whan api requires authkey is set to true);
	*/
	public function __construct($name,$image_url,$search,$requires_authkey=false,$master=NULL,$api_keys=NULL){
		$this->name=$this->validate_string($name);//set name and validate it
		$this->image_url=parent::validate_url($image_url);//validate url
		$requires_authkey=$this->validate_bool($requires_authkey);//clean requires authkey
		$this->requires_authkey=$requires_authkey;//set requires authkey as field
		$this->api_keys=($requires_authkey)?$api_keys:new api_keys($this);//set a new api key for  the api.
		if($requires_authkey&&is_a($this->api_keys,"\shopping_agg\api\api_keys")){
			$this->api_keys->remap_master($this);//ensure api_keys master is api
		}
		$this->search=$this->type_check($search, "\shopping_agg\interaction\interaction")?$search:null;//ensure the search object is an \shopping_agg\interaction\interaction
		if(is_a($this->search, "shopping_agg\interaction\interaction")){//if the check didnt fail
			$this->search->remap_master($this);//remap the master
		}
	}
	/**
	 * This is the overides the request class inheretence to signify that ths is the root node and acts as an alias for curl run;
	 * @see \api\api::curl_run()
	 * @see \api\interactinal_class::request()
	 */
	public function request($options){
		return $this->curl_run($options);//invoke and return the result of an inturnal method (curl_run)
	}
	/**
	 * This method validates that the search property is a valid interacion class
	 * @param \interaction\interaction|mixed $search The property to check.
	 * @return \interaction\interaction
	 * 
	 */
	private function validate_search($search){
		if(!$this->type_check($search,"\\shopping_agg\\interacton\\interacton")){//if search is an invalid interaction object
			return new interaction($this,null,null,null);//replace it with an empty interaction object
		}
		return $search;
	}
	//Sorry I'm lazy an reused documentation
	/**
	 * This is the function that handles the final execution of sending out requests to api endpoints (and is the only defined method that does so)
	 * @see \shopping_agg\api\interactinal_class::request()
	 * @see \API_CURL_DEFAULTS 
	 *///[Inturnal_break]
	private function curl_run($options){
		$defaults=unserialize(API_CURL_DEFAULTS);
		foreach($defaults as $key=>$value){//Merge defined keyword arguments with var defaults
			$options[$key]=(isset($options[$key]))?$options[$key]:$defaults[$key];//preference options over defaults
		}
		//PRE PROCESSING VALIDATION;
		foreach(unserialize(API_CURL_REQUIREMENTS) as $required){//iterate through required options.
			if(!isset($options[$required])){//if a critical value is not passed
				\shopping_agg\error_handle\error_log_c("Error: curl options passed without one or more critical arguments. missing: $required \n required defaults are: ".implode("\n", unserialize(API_CURL_REQUIREMENTS)),$options);//throw an error
				return false;//return error
			}
		}
		$url=parse_url($options["endpoint"]);//ensure URL is valid (as an api endpoint)
		if(!$url){//if url is invalid
			\shopping_agg\error_handle\error_log_c("Error: cURL request aborted:\n".var_export($options["url"]." cannot be parsed as a url (via the built-in method parse_url(\$url)) and threfere has resulted in the request bieng aborted.",true),$options["url"] );//Log an error...
			return false;//and throw error
		}
		//@todo add type checks that data objects are associative arrays.
		//MERGE REQUEST DATA
		if(sizeof($options["data"])!==0){//if data exsists
			 if (strtoupper($options["method"])=="POST"){//if the method is going to be sent as a post request
			 	$options["post_data"]=array_merge($options["post_data"],$options["data"]);//merge core data with post request data;
			 }
			 elseif(strtoupper($options["method"])=="GET"){//if data is get data
			 	$options["get_data"]=array_merge($options["get_data"],$options["data"]);//merge core data with get request data;
			 }
		}
		//GET API KEYS
		if($this->requires_authkey){//if api requires api key
			$api_key=$this->api_keys->get_key($options["api_key"]);//get the api key
			if(!$this->type_check($api_key, "shopping_agg\api\api_key")){//check that it is valid
				return false;
			};
			$method=(strtoupper($api_key->method)=="POST")?"post_data":"get_data";//get correct method
			$options[$method]=array_merge($options[$method],$api_key->get_key_value_pair());//Build access key call by way of "new acess key"= & or "" 
			//dependent on if the current querey string has any content in it or not joind with the url 
			//encoded api key with refrence to the key required. 
		}
		//BUILD CONFUGURATION OPTIONS
		$request=curl_init();//initilize request
		if(strtoupper($options["method"])=="POST"||count($options["post_data"])!=0){//if it is a post request or post data exsists
			curl_setopt($request, CURLOPT_POST, true);//enables http post
		}
		if(count($options["post_data"])!=0){//if post data exsists
			curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($options["post_data"]));//add it to post data
		}
		$endpoint_components=parse_url($options["endpoint"]);//parse the url
		$curl_request_quereystring=isset($endpoint_components["query"])?$endpoint_components["query"]:"";//define http querey string for later use (if querey string exsists set it to that)
		if(count($options["get_data"])!=0){//if there is querey data
			$curl_request_quereystring.=(strlen($curl_request_quereystring)!=0)?"&":"".http_build_query($options["get_data"]);//add it to the querey string
		}
		//(count($curl_request_quereystring)!=0)?"&":""
		//CONSTRUCT FINAL URL
		$final_url=((strpos( $options["endpoint"], "?")===false)?$options["endpoint"]:preg_replace('/(\?.*)/', '', $options["endpoint"],1)).//remove the pre-exsisting querey string
		((strlen($curl_request_quereystring)!=0)?"?":"").//put a question mark in if a querey string exsists
				$curl_request_quereystring;//attach prebuilt querey string
		if($options["pre_request_function"]!==""){//if a pre request function is set
			$options["pre_request_function"]=explode("[Inturnal_break]", $options["pre_request_function"],2);//break apart arguments and function array
			if(count($options["pre_request_function"])==2){
				//$options["pre_request_function"][0]=function name
				//$options["pre_request_function"][1]=function arguments
				$result_of_function_call=call_user_func_array($options["pre_request_function"][0], array_merge(array(array_merge(array("final_url"=>$final_url),$options)),explode(",",$options["pre_request_function"][1])));//make call to pre-request function with in the forms function($options+$final_url,$extra_options)
				if(filter_var($result_of_function_call,FILTER_VALIDATE_URL)){//validate url
					$final_url=$result_of_function_call;
				}
				else{
					\shopping_agg\error_handle\error_log_c("Error: pre-request function didn't result in a valid url.", $result_of_function_call);//log error.
				}
			}
			else{
				\shopping_agg\error_handle\error_log_c("Error: Unable to properly call pre request function due to faulty encoding. ('[Inturnal_break]' missing)",$options["pre_request_function"]);//log error.
			}
			
		}
		shopping_agg\debug\debug_print("Full request uri: <hr/>".$final_url."<hr/>");
		//SET CONSTANT CURL OPTIONS
		curl_setopt($request, CURLOPT_URL, $final_url);//set final url
		curl_setopt($request,CURLOPT_RETURNTRANSFER,true);//allways get data returned from api
		//SET DEFAULT CURL OPTIONS
		curl_setopt_array($request, unserialize(CURL_DEFAULTS));//implament default curl options
		//LAUNCH REQUEST
		//$response=curl_exec($request);
		$response=curl_exec($request);
		if($response===false){//on error
			\shopping_agg\error_handle\error_log_c("Error: request failed. Error reported as '".(string)curl_error($request)."' (error code:'".(string)curl_errno($request)."').",curl_getinfo($request));
			return false;
		}
		$http_status=intval(curl_getinfo($request, CURLINFO_HTTP_CODE));//look at the status code
		if($http_status!==200){
			$codes = unserialize(HTTP_STATUS_CODES);
			$code_msg=(isset($codes[$http_status]))?$codes[$http_status]:"Unknown status code.";//if there is an error code set get the msg (otehrwise get nothing)
			\shopping_agg\error_handle\error_log_c("Error: request returned http stats code $http_status:".$code_msg,curl_getinfo($request));
			if (strlen($response)!=0&&DEBUGGING){//takes alot of resources to calculate this stuff
				shopping_agg\debug\debug_print("Response:<hr/>$response<hr/>");//src='data:text/html;charset=utf-8,".base64_encode($response)."'></iframe>");
			}
			return false;//return error
		}
		//var_dump(json_decode(json_encode(@simplexml_load_string($response)),true));
		//catch(\Error $e){}
		return $response;//return false
	}
	
	
}
