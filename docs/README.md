# Shopping-agg"

## Table of Contents

* [api](#api)
    * [get_root_node](#get_root_node)
    * [__call](#__call)
    * [handle_invisible_method_call](#handle_invisible_method_call)
    * [request](#request)
    * [__construct](#__construct)
    * [set_next_ready_state_to_unready](#set_next_ready_state_to_unready)
    * [set_next_ready_state_to_ready](#set_next_ready_state_to_ready)
    * [get_last_data](#get_last_data)
    * [set_next_url](#set_next_url)
    * [get_next_data](#get_next_data)
    * [clear_last](#clear_last)
    * [get_root_node](#get_root_node-1)
* [api_key](#api_key)
    * [get_root_node](#get_root_node-2)
    * [__call](#__call-1)
    * [handle_invisible_method_call](#handle_invisible_method_call-1)
    * [request](#request-1)
    * [__construct](#__construct-1)
    * [get_name](#get_name)
    * [get_value](#get_value)
    * [is_default](#is_default)
    * [get_key_value_pair](#get_key_value_pair)
    * [make_default](#make_default)
    * [remap_master](#remap_master)
* [api_keys](#api_keys)
    * [get_root_node](#get_root_node-3)
    * [__call](#__call-2)
    * [handle_invisible_method_call](#handle_invisible_method_call-2)
    * [request](#request-2)
    * [__construct](#__construct-2)
    * [remap_master](#remap_master-1)
    * [set_key](#set_key)
    * [set_default](#set_default)
    * [has_default](#has_default)
    * [get_defualt](#get_defualt)
    * [get_key](#get_key)
* [endpoint](#endpoint)
    * [get_root_node](#get_root_node-4)
    * [__call](#__call-3)
    * [handle_invisible_method_call](#handle_invisible_method_call-3)
    * [request](#request-3)
    * [__construct](#__construct-3)
    * [remap_master](#remap_master-2)
    * [build_configuration_array](#build_configuration_array)
    * [set_endpoint](#set_endpoint)
    * [ready](#ready)
* [interaction](#interaction)
    * [get_root_node](#get_root_node-5)
    * [__call](#__call-4)
    * [handle_invisible_method_call](#handle_invisible_method_call-4)
    * [request](#request-4)
    * [__construct](#__construct-4)
    * [get_endpoint](#get_endpoint)
    * [set_response](#set_response)
    * [set_endpoint](#set_endpoint-1)
    * [exec_request](#exec_request)
    * [ready](#ready-1)
    * [remap_master](#remap_master-3)
* [response](#response)
    * [get_root_node](#get_root_node-6)
    * [__call](#__call-5)
    * [handle_invisible_method_call](#handle_invisible_method_call-5)
    * [request](#request-5)
    * [__construct](#__construct-5)
    * [remap_master](#remap_master-4)
    * [handle_response](#handle_response)
    * [ready](#ready-2)

## api

This is the core class for the intaractional layer and contains all information neccacery for interactions with an api.



* Full name: \shopping_agg\api\api
* Parent class: \shopping_agg\api\interactinal_class


### get_root_node



```php
api::get_root_node(  ): \shopping_agg\api\api
```

This gets the root node of an api tree (the api class)





---

### __call



```php
api::__call(  $method_name,  $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **** |  |
| `$args` | **** |  |




---

### handle_invisible_method_call

The method as defiend for interactional classes. This allows for the automatic callng of methods of of parent classes without the need for refeincing a chain of masters.<br/>This will terminate at the first master that has that method defined.<br/>This should only be invoked by way of the __call() magic method and not called directly.<br/>  WARNING THIS METHOD USES EVAL HANDLE WITH CARE.

```php
api::handle_invisible_method_call( string $method_name, array $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **string** | undefined method name |
| `$args` | **array** | arguments passed to the undefiend method. |




---

### request

This is the overides the request class inheretence to signify that ths is the root node and acts as an alias for curl run;

```php
api::request( array $options )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$options` | **array** | This is an associative array of options for the request. The options are as follows are as follows:
['endpoint'] => string $endpoint This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.(REQUIRED)
['method'] => string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE")
['post_data'] => string[] $post_data associative array, of single layer depth, that will be send as form parameters.
['get_data'] => string[] $get_data associative array, of single layer depth, that will be send as a query string.
['data'] => string[] $data associative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.)
['api_key'] => string $api_key The name of the api_key to use (if API key not required don't define, if default is needed set to "DEFAULT")
 ['pre_request_function'] => string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
 ['delay']=> int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
 ['tries']=> int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1)
Please refer to the API curl request handle documentation. |



**See Also:**

* \api\api::curl_run() * \api\interactinal_class::request() 

---

### __construct

This is the constructor method for the api class

```php
api::__construct( string $name, string $image_url, \shopping_agg\interaction\interaction $search, boolean $requires_authkey = false, \shopping_agg\api\unknown $master = NULL,  $api_keys = NULL, \shopping_agg\interaction\interaction $next = NULL )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$name` | **string** | The name of the api |
| `$image_url` | **string** | The full url link to image for the logo of the api |
| `$search` | **\shopping_agg\interaction\interaction** | The main search object |
| `$requires_authkey` | **boolean** | If this api is open of requires an authentication key |
| `$master` | **\shopping_agg\api\unknown** | The master object to the api root node (Yet to be implamened) |
| `$api_keys` | **** |  |
| `$next` | **\shopping_agg\interaction\interaction** | The main interaction object to govern fetching more data items from the api |




---

### set_next_ready_state_to_unready

Sets the ready state of the next object to unready so it cannot be called. (request cannot be launched)

```php
api::set_next_ready_state_to_unready(  )
```







---

### set_next_ready_state_to_ready

Sets the ready state of the next object to ready so it can be called. (request can be launched)

```php
api::set_next_ready_state_to_ready(  )
```







---

### get_last_data

Gets the options used last set of calls

```php
api::get_last_data( integer $index ): \shopping_agg\api\$options
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$index` | **integer** | The index of the data to retrieve |


**Return Value:**

The set of options used previousy



---

### set_next_url

This method sets the next url for the api endpoint

```php
api::set_next_url(  $url )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$url` | **** |  |




---

### get_next_data

This function executes the next request as defined by the aggragator.

```php
api::get_next_data(  ): mixed
```





**Return Value:**

the response data



---

### clear_last

This method clears the 'last' field and replaces it with an empty array

```php
api::clear_last(  )
```







---

### get_root_node

Gets root node of interactional class tree (real and not boud to __call) Passes by reference
{@inheritDoc}

```php
api::get_root_node(  )
```






**See Also:**

* \shopping_agg\api\interactinal_class::get_root_node() 

---

## api_key

The commen facets of all interactional objects including overflow methods



* Full name: \shopping_agg\api\api_key
* Parent class: \shopping_agg\api\interactinal_class


### get_root_node



```php
api_key::get_root_node(  ): \shopping_agg\api\api
```

This gets the root node of an api tree (the api class)





---

### __call



```php
api_key::__call(  $method_name,  $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **** |  |
| `$args` | **** |  |




---

### handle_invisible_method_call

The method as defiend for interactional classes. This allows for the automatic callng of methods of of parent classes without the need for refeincing a chain of masters.<br/>This will terminate at the first master that has that method defined.<br/>This should only be invoked by way of the __call() magic method and not called directly.<br/>  WARNING THIS METHOD USES EVAL HANDLE WITH CARE.

```php
api_key::handle_invisible_method_call( string $method_name, array $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **string** | undefined method name |
| `$args` | **array** | arguments passed to the undefiend method. |




---

### request

This allows for passing of request to the base of the api tree for request processing

```php
api_key::request( array $options )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$options` | **array** | This is an associative array of options for the request. The options are as follows are as follows:
['endpoint'] => string $endpoint This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.(REQUIRED)
['method'] => string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE")
['post_data'] => string[] $post_data associative array, of single layer depth, that will be send as form parameters.
['get_data'] => string[] $get_data associative array, of single layer depth, that will be send as a query string.
['data'] => string[] $data associative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.)
['api_key'] => string $api_key The name of the api_key to use (if API key not required don't define, if default is needed set to "DEFAULT")
 ['pre_request_function'] => string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
 ['delay']=> int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
 ['tries']=> int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1)
Please refer to the API curl request handle documentation. |



**See Also:**

* \shopping_agg\api\api::curl_run 

---

### __construct



```php
api_key::__construct( \shopping_agg\api\api_keys $master, string $name, string $value, string $method = &quot;GET&quot;, boolean $default = False )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **\shopping_agg\api\api_keys** | the master of this object. should be n instance of the api_keys class |
| `$name` | **string** | The name of the api key (The name of it as per the API and not your naming sence!!!!) |
| `$value` | **string** | The value of the api key or secret |
| `$method` | **string** | The method of sending the key to the api (default GET) |
| `$default` | **boolean** | Wether this is the default key or not. This must be set true for one key per the api (No more or less) |




---

### get_name

Gets the name of a given api key

```php
api_key::get_name(  ): string
```





**Return Value:**

$name the name of the api key



---

### get_value

Gets the value of a given api key

```php
api_key::get_value(  ): string
```





**Return Value:**

$value the value of the api key



---

### is_default

Returns if the api_key is default.

```php
api_key::is_default(  )
```







---

### get_key_value_pair

This compiles the name and value of the api key into an associative array of a key value pairing,

```php
api_key::get_key_value_pair(  ): array
```





**Return Value:**

([name]=>value)



---

### make_default

This makes an api key default (Please note that only one api key instance in the api keys class instance should be a default key as any more than one may start to cause errors.)

```php
api_key::make_default(  )
```







---

### remap_master

This method remaps the master of the api_keys object to a new api class instance.

```php
api_key::remap_master( \shopping_agg\api\api_keys $master )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **\shopping_agg\api\api_keys** | The new api keys class instance to handle as master |




---

## api_keys

The commen facets of all interactional objects including overflow methods



* Full name: \shopping_agg\api\api_keys
* Parent class: \shopping_agg\api\interactinal_class


### get_root_node



```php
api_keys::get_root_node(  ): \shopping_agg\api\api
```

This gets the root node of an api tree (the api class)





---

### __call



```php
api_keys::__call(  $method_name,  $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **** |  |
| `$args` | **** |  |




---

### handle_invisible_method_call

The method as defiend for interactional classes. This allows for the automatic callng of methods of of parent classes without the need for refeincing a chain of masters.<br/>This will terminate at the first master that has that method defined.<br/>This should only be invoked by way of the __call() magic method and not called directly.<br/>  WARNING THIS METHOD USES EVAL HANDLE WITH CARE.

```php
api_keys::handle_invisible_method_call( string $method_name, array $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **string** | undefined method name |
| `$args` | **array** | arguments passed to the undefiend method. |




---

### request

This allows for passing of request to the base of the api tree for request processing

```php
api_keys::request( array $options )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$options` | **array** | This is an associative array of options for the request. The options are as follows are as follows:
['endpoint'] => string $endpoint This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.(REQUIRED)
['method'] => string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE")
['post_data'] => string[] $post_data associative array, of single layer depth, that will be send as form parameters.
['get_data'] => string[] $get_data associative array, of single layer depth, that will be send as a query string.
['data'] => string[] $data associative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.)
['api_key'] => string $api_key The name of the api_key to use (if API key not required don't define, if default is needed set to "DEFAULT")
 ['pre_request_function'] => string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
 ['delay']=> int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
 ['tries']=> int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1)
Please refer to the API curl request handle documentation. |



**See Also:**

* \shopping_agg\api\api::curl_run 

---

### __construct

constructor method for array keys

```php
api_keys::__construct( \shopping_agg\api\api $master, array $keys = array() )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **\shopping_agg\api\api** | REQUIRED the master node in the api tree to bind this to (should be the root node) |
| `$keys` | **array** | array of api key objects OPTINAL there will need to be a defiend array of api_key objects for this to be passed as valid and will act as the initial store of access keys for the api |




---

### remap_master

This method remaps the master of the api_keys object to a new api class instance.

```php
api_keys::remap_master( \shopping_agg\api\shopping_agg\api\api $master )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **\shopping_agg\api\shopping_agg\api\api** | The new api keys class instance to handle as master |




---

### set_key

Add a new key to the api

```php
api_keys::set_key(  $value ): boolean
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$value` | **** |  |


**Return Value:**

false on error true on success



---

### set_default

Set the default key for any given api keys instance

```php
api_keys::set_default( \shopping_agg\api\api_key $key ): boolean
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$key` | **\shopping_agg\api\api_key** |  |




---

### has_default

Gets wether there is an exsisting default key for this set of api keys

```php
api_keys::has_default(  ): boolean
```





**Return Value:**

true if the api_keys instance has a default key and false otherwise.



---

### get_defualt

This gets the default key from the api_keys instance

```php
api_keys::get_defualt(  ): \shopping_agg\api\api_key|boolean
```





**Return Value:**

(returns false on error)



---

### get_key

gets key with refrenced name

```php
api_keys::get_key( string $key_name = &quot;&quot; ): \shopping_agg\api\api_key
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$key_name` | **string** | OPTINAL (in case of ther only being one stored key) The name of the key to get from the key store. |


**Return Value:**

on succsuess NULL on error.



---

## endpoint

The commen facets of all interactional objects including overflow methods



* Full name: \shopping_agg\interaction\endpoint
* Parent class: \shopping_agg\api\interactinal_class


### get_root_node



```php
endpoint::get_root_node(  ): \shopping_agg\api\api
```

This gets the root node of an api tree (the api class)





---

### __call



```php
endpoint::__call(  $method_name,  $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **** |  |
| `$args` | **** |  |




---

### handle_invisible_method_call

The method as defiend for interactional classes. This allows for the automatic callng of methods of of parent classes without the need for refeincing a chain of masters.<br/>This will terminate at the first master that has that method defined.<br/>This should only be invoked by way of the __call() magic method and not called directly.<br/>  WARNING THIS METHOD USES EVAL HANDLE WITH CARE.

```php
endpoint::handle_invisible_method_call( string $method_name, array $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **string** | undefined method name |
| `$args` | **array** | arguments passed to the undefiend method. |




---

### request

This allows for passing of request to the base of the api tree for request processing

```php
endpoint::request( array $options )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$options` | **array** | This is an associative array of options for the request. The options are as follows are as follows:
['endpoint'] => string $endpoint This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.(REQUIRED)
['method'] => string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE")
['post_data'] => string[] $post_data associative array, of single layer depth, that will be send as form parameters.
['get_data'] => string[] $get_data associative array, of single layer depth, that will be send as a query string.
['data'] => string[] $data associative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.)
['api_key'] => string $api_key The name of the api_key to use (if API key not required don't define, if default is needed set to "DEFAULT")
 ['pre_request_function'] => string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
 ['delay']=> int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
 ['tries']=> int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1)
Please refer to the API curl request handle documentation. |



**See Also:**

* \shopping_agg\api\api::curl_run 

---

### __construct

Constructor method for the endpoint class.

```php
endpoint::__construct( \shopping_agg\interaction\intercation\interaction $master = null, string $endpoint = &quot;&quot;, string $method = &quot;GET&quot;, array&lt;mixed,string&gt; $post_data = array(), array&lt;mixed,string&gt; $get_data = array(), array&lt;mixed,string&gt; $data = array(), string $api_key = &quot;&quot;, string $pre_request_function = &quot;&quot;, integer $delay, integer $tries = 1 )
```

All permiters are optional but must be defiend by the time that the request execute method is called on it or requests sent from it will scilently fail.


**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **\shopping_agg\interaction\intercation\interaction** | the master for any instance endpoint class should only ever be the related ineraction class instance. |
| `$endpoint` | **string** | This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking. |
| `$method` | **string** | The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE") |
| `$post_data` | **array<mixed,string>** | assoiative array, of single layer depth, that will be send as form peramiters. |
| `$get_data` | **array<mixed,string>** | assoiative array, of single layer depth, that will be send as a querey string. |
| `$data` | **array<mixed,string>** | assoiative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.) |
| `$api_key` | **string** | The name of the api_key to use (if api key not required dont define, if default is needed set to "DEFAULT") |
| `$pre_request_function` | **string** | This is the function to call in the case calling before the final request is sent (in the same format as special vars). |
| `$delay` | **integer** | The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0) |
| `$tries` | **integer** | The number of times to attempt to connect in total in the event of a failed connection (default 1) |




---

### remap_master

This method is used to remap the master of endpoint objects

```php
endpoint::remap_master( \shopping_agg\interaction\interaction $master )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **\shopping_agg\interaction\interaction** |  |




---

### build_configuration_array

This fuction builds and returns the configuration options data stored in this object instance

```php
endpoint::build_configuration_array(  ): array&lt;mixed,string&gt;
```





**Return Value:**

$options The array of configuration options to follow.



---

### set_endpoint

set the endpoint for the endpoint class instance.

```php
endpoint::set_endpoint( string $endpoint )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$endpoint` | **string** | the url to set as the endpoint (This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.) |




---

### ready

This checks the state of the endpoint object and evaluates if it is in a reasonable state to send off with.

```php
endpoint::ready( boolean $errors = false ): boolean
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$errors` | **boolean** | If to display log error on calls or not (will log error on $errors being true)(false otherwise) |


**Return Value:**

$ready Returns true on the class bieng ready and false otherwise.



---

## interaction

The commen facets of all interactional objects including overflow methods



* Full name: \shopping_agg\interaction\interaction
* Parent class: \shopping_agg\api\interactinal_class


### get_root_node



```php
interaction::get_root_node(  ): \shopping_agg\api\api
```

This gets the root node of an api tree (the api class)





---

### __call



```php
interaction::__call(  $method_name,  $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **** |  |
| `$args` | **** |  |




---

### handle_invisible_method_call

The method as defiend for interactional classes. This allows for the automatic callng of methods of of parent classes without the need for refeincing a chain of masters.<br/>This will terminate at the first master that has that method defined.<br/>This should only be invoked by way of the __call() magic method and not called directly.<br/>  WARNING THIS METHOD USES EVAL HANDLE WITH CARE.

```php
interaction::handle_invisible_method_call( string $method_name, array $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **string** | undefined method name |
| `$args` | **array** | arguments passed to the undefiend method. |




---

### request

This allows for passing of request to the base of the api tree for request processing

```php
interaction::request( array $options )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$options` | **array** | This is an associative array of options for the request. The options are as follows are as follows:
['endpoint'] => string $endpoint This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.(REQUIRED)
['method'] => string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE")
['post_data'] => string[] $post_data associative array, of single layer depth, that will be send as form parameters.
['get_data'] => string[] $get_data associative array, of single layer depth, that will be send as a query string.
['data'] => string[] $data associative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.)
['api_key'] => string $api_key The name of the api_key to use (if API key not required don't define, if default is needed set to "DEFAULT")
 ['pre_request_function'] => string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
 ['delay']=> int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
 ['tries']=> int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1)
Please refer to the API curl request handle documentation. |



**See Also:**

* \shopping_agg\api\api::curl_run 

---

### __construct



```php
interaction::__construct( mixed $master, \shopping_agg\interaction\interaction\endpoint $endpoint, \shopping_agg\interaction\interaction\response $response, \shopping_agg\interaction\unknown $errors )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **mixed** | The master of this class instance (not limited to any class) |
| `$endpoint` | **\shopping_agg\interaction\interaction\endpoint** | This should be the related api endpoint class |
| `$response` | **\shopping_agg\interaction\interaction\response** |  |
| `$errors` | **\shopping_agg\interaction\unknown** |  |




---

### get_endpoint

Gets and returns the set api endpoint

```php
interaction::get_endpoint(  ): \shopping_agg\interaction\endpoint
```





**Return Value:**

$endpoint


**See Also:**

* \interaction\interaction::$endpoint 

---

### set_response

This sets the state of the response variable

```php
interaction::set_response( \interaction\response $response )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$response` | **\interaction\response** | The response object to update |




---

### set_endpoint



```php
interaction::set_endpoint(  $endpoint )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$endpoint` | **** |  |




---

### exec_request

This method exexutes the cURL request by calling the request method and building the configuration array

```php
interaction::exec_request( null|boolean $handle_response = true, boolean $errors = false ): string
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$handle_response` | **null&#124;boolean** | if the response should be handled by the predefined request methods or not. |
| `$errors` | **boolean** | if errors should be logged or not. |


**Return Value:**

$respone The responese  data of the cURL request



---

### ready

This function evaluates if the interaction clas is in a state that is redy to lunch the cURL request.

```php
interaction::ready( boolean $errors = false ): boolean
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$errors` | **boolean** | Wether to launch errorsin the event of a false call or not. |


**Return Value:**

$ready (true on ready; false otherwise)



---

### remap_master

This method is used to remap the master of interaction objects

```php
interaction::remap_master( \shopping_agg\interaction\shopping_agg\api\api|\shopping_agg\interaction\shopping_agg\interaction\response $master )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **\shopping_agg\interaction\shopping_agg\api\api&#124;\shopping_agg\interaction\shopping_agg\interaction\response** | The master to set this object to have |




---

## response

The commen facets of all interactional objects including overflow methods



* Full name: \shopping_agg\interaction\response
* Parent class: \shopping_agg\merge\merge

**See Also:**

* \shopping_agg\interaction\shopping_agg\merge\merge 

### get_root_node



```php
response::get_root_node(  ): \shopping_agg\api\api
```

This gets the root node of an api tree (the api class)





---

### __call



```php
response::__call(  $method_name,  $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **** |  |
| `$args` | **** |  |




---

### handle_invisible_method_call

The method as defiend for interactional classes. This allows for the automatic callng of methods of of parent classes without the need for refeincing a chain of masters.<br/>This will terminate at the first master that has that method defined.<br/>This should only be invoked by way of the __call() magic method and not called directly.<br/>  WARNING THIS METHOD USES EVAL HANDLE WITH CARE.

```php
response::handle_invisible_method_call( string $method_name, array $args )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$method_name` | **string** | undefined method name |
| `$args` | **array** | arguments passed to the undefiend method. |




---

### request

This allows for passing of request to the base of the api tree for request processing

```php
response::request( array $options )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$options` | **array** | This is an associative array of options for the request. The options are as follows are as follows:
['endpoint'] => string $endpoint This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.(REQUIRED)
['method'] => string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE")
['post_data'] => string[] $post_data associative array, of single layer depth, that will be send as form parameters.
['get_data'] => string[] $get_data associative array, of single layer depth, that will be send as a query string.
['data'] => string[] $data associative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.)
['api_key'] => string $api_key The name of the api_key to use (if API key not required don't define, if default is needed set to "DEFAULT")
 ['pre_request_function'] => string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
 ['delay']=> int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
 ['tries']=> int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1)
Please refer to the API curl request handle documentation. |



**See Also:**

* \shopping_agg\api\api::curl_run 

---

### __construct

Constructor method for response class.

```php
response::__construct( string $response_type = &quot;JSON&quot;, string $pivot = &quot;&quot;, array $vars_to_look_for = array(), \shopping_agg\interaction\interaction $master = NULL )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$response_type` | **string** | The expected document resonse type ["JSON","XML" or "JSONP"] |
| `$pivot` | **string** | The pivot leading to the array containing items |
| `$vars_to_look_for` | **array** | The variables to look for |
| `$master` | **\shopping_agg\interaction\interaction** | The master to assign to this response class instance |




---

### remap_master

This method is used to remap the master of endpoint objects

```php
response::remap_master( \shopping_agg\interaction\interaction|\shopping_agg\interaction\\response $master )
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$master` | **\shopping_agg\interaction\interaction&#124;\shopping_agg\interaction\\response** |  |




---

### handle_response

This method is set to handle the response of an api request

```php
response::handle_response( string|\shopping_agg\interaction\stdClass|array $data ): mixed
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$data` | **string&#124;\shopping_agg\interaction\stdClass&#124;array** | The data to handle the response of |


**Return Value:**

the handled data (false on error)



---

### ready

This checks the state of the response object and evaluates if it is in a reasonable state to send off with.

```php
response::ready( boolean $errors = false ): boolean
```




**Parameters:**

| Parameter | Type | Description |
|-----------|------|-------------|
| `$errors` | **boolean** | If to display log error on calls or not (will log error on $errors being true)(false otherwise) |


**Return Value:**

$ready Returns true on the class bieng ready and false otherwise.



---



--------
> This document was automatically generated from source code comments on 2017-10-24 using [phpDocumentor](http://www.phpdoc.org/) and [cvuorinen/phpdoc-markdown-public](https://github.com/cvuorinen/phpdoc-markdown-public)
