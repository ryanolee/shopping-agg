<?php
/**
 * This document defines the api_key class 
 * @author rizza <rizza@rizza.net>
 * @see api\api_key
 * @package shopping_aggregator\interactional_layer\api_keys\api_key
 */

namespace shopping_agg\api;//define that this function is part of the api namespace

use shopping_agg;

/**
 * 
 * @author rizza
 * @version 1.0
 * The api_key class stores all the prerequsite data for a single api key and acts as a slave object genarator for the api_keys master instance.
 * @property string $name The name of the api key (to be used the varible name of the api_key).
 * @property string $value The value of th api key to use.
 * @property string $method The method of sending the api_key needs to be either "POST" or "GET" (other methods are unsupported.)
 * @property bool $default If the class is default or not. 
 */

class api_key extends interactinal_class{
	private $name,$value;
	public $method,$default;
	/**
	 * @param api_keys $master the master of this object. should be n instance of the api_keys class
	 * @param string $name The name of the api key (The name of it as per the API and not your naming sence!!!!)
	 * @param string $value The value of the api key or secret
	 * @param string $method The method of sending the key to the api (default GET)
	 * @param bool $default Wether this is the default key or not. This must be set true for one key per the api (No more or less)
	 */
	public function __construct($master,$name,$value,$method="GET",$default=False){
		//BEGIN DATATYPE TESTS
		$data=$this->check_name_val($name,$value);//do validatons for name and value
		$this->name=$data[0];//assign returned data
		$this->value=$data[1];//assign returned data
		$this->method=$this->check_method($method);//check the method of sending.
		$this->default=$this->check_default($default);//check if the variable is default.
		$this->master=$this->check_master($master);//check if master is an isntance of the master class;
		//END DATATYPE TESTS
	}
	/**
	 * Gets the name of a given api key
	 * @return string $name the name of the api key
	 */
	public function get_name(){
		return $this->name;//return name of the api_key instance
	}
	/**
	 * Gets the value of a given api key
	 * @return string $value the value of the api key
	 */
	public function get_value(){
		return $this->value;//return name of the api_key instance
	}
	/**
	 * Returns if the api_key is default.
	 * @returns bool (True if the api key is default; false otherwise)
	 */
	public function is_default(){
		return $this->default;//returns if key is default or not
	}
	/**
	 * This compiles the name and value of the api key into an associative array of a key value pairing,
	 * @return array ([name]=>value)
	 */
	public function get_key_value_pair(){
		return array($this->get_name()=>$this->get_value());//compile ascociative array
		
	}
	/**
	 * This makes an api key default (Please note that only one api key instance in the api keys class instance should be a default key as any more than one may start to cause errors.)
	 */
	public function make_default(){
		$this->default=true;
		
	}
	/**
	 * This method remaps the master of the api_keys object to a new api class instance.
	 * @param shopping_agg\api\api_keys $master The new api keys class instance to handle as master
	 */
	public function remap_master($master){
		return $this->remap_master_handle($master,"shopping_agg\api\api_keys");
	}
	/**
	 * validates that both name and value are api keys (if not errors will be logged.)
	 * @param string $name the name of the api key
	 * @param string $value the value of the api key
	 * @returns array $data in the form array($name,$value)
	 */
	private function check_name_val($name="",$value=""){
		if(!is_string($name)||!is_string($value)){//if the name of an api key is not a string or its value.
			if(LOGGING){//if logging is enabled
				\shopping_agg\error_handle\error_log_c("Error: \$name(".gettype($name).") or \$value(".gettype($value).") passed to the api_keys constructor method as a non string. They will be attempted to be converted into strings but otherhwise be passed as null strings");
				//log the given error
			}
			try{//try to...
				$name="$name";//convert name to a string and..
				$value="$value";//convert value to a string.
			}
			catch(Exception $e){//if that is not possible
				$focus=(is_string($value))?$name:$value;//get focus on item that failed to convert
				if(LOGGING){//log error related to that if logging is enabled
					\shopping_agg\error_handle\error_log_c("Error:unable to convert :".var_export($focus,true)." to a string.\n object is not fit for perpose.");//log error
				}
				$name="Error";//uniqe id
				$value="ERROR:reffer to error logs";// tell dev to look at error logs
			}
			
		}
		return array($name,$value);
	}
	/**
	 * Checks if the method passed to one of the class functions is valid for inturnal use. (if not errors will be logged.)
	 * @param string $method Must be a valid HTTP request method ("POST,GET")
	 * @return string $method the either corrected of same method as passed in.
	 */
	private function check_method($method){
		if(!is_string($method)){//if the method is not a string
			if(LOGGING){//if logging is enabled
				\shopping_agg\error_handle\error_log_c("Error:\$method passed as non string to api_keys class please pass non string reverting method to default ('GET')",$method);//log error
			}
			$method="GET";//revert to default method
		}
		if(!in_array(strtoupper($method),array("GET","POST"))){
			if(LOGGING){//if logging is enabled
				\shopping_agg\error_handle\error_log_c("Error:\$method passed as invalid type ($method) to api_keys class please pass method in range of (POST,GET)\nReverting method to default ('GET')", $method);//log error
			}
			$method ="GET";//revert to default method of defining querey strings
		}
		return strtoupper($method);//return method
	}
	/**
	 * Checks if the default state of the api key passed to one of the class functions is valid for inturnal use. (if not errors will be logged.)
	 * @param bool $default the value to check if it is a boolean
	 * @return string $method the either corrected of same $default variable as passed in.
	 */
	private function check_default($default){
		if(!is_bool($default)){//if default is not set to a boolean
			if(LOGGING){//if logging is enabled
				\shopping_agg\error_handle\error_log_c("ERROR: \$defualt pass as non beoolean. defaulting the api key to be non-default key.",$default);
				//log the error
			}
			$default=false;//revert the default value to its default value
		}
		return $default;//return that default value
	}
	/**
	 * See if the master is set to an instance of the api_keys class:
	 * @param api_keys $master the object to check if it is a api keys class
	 * @return mixed $master will return the original variable passed to it but if logging is enabled it will warn people that YOU ARE NOT SOPPOSED DO THAT :(.
	 * @see api_keys
	 */
	private function check_master($master){
		if((!is_a($master,"shopping_agg\api\api_keys")&&!is_null($master))&&LOGGING){//if the master is invalid and logging enabled
			\shopping_agg\error_handle\error_log_c("WARNING: \$master of api_key passed a ".gettype($master)." and not a instance of the api_keys class.\nThis will result in unpredicatable and most likley erronious behaviour. ",$master);
			//log a warning about possibly broken behaviour on part of the api
		}
		return $master;//return master (begrudginly if it is not an isntance of the api_keys class)
	}
	
}