<?php
/**
 * This document defines the abstract class that holds combined traits of all interactional classes.
 * @author rizza <rizza@rizza.net>
 * @package shopping_aggregator\interactional_layer\abstract
 *
 */

namespace shopping_agg\api;
/**
 * The commen facets of all interactional objects including overflow methods
 * @author rizza
 * @method \shopping_agg\api\api get_root_node() This gets the root node of an api tree (the api class)
 */
abstract class interactinal_class{
	//use  \shopping_agg\debug\private_debug_call;
	public function __call($method_name,$args){
		if(in_array($method_name, unserialize(ALLOWED_OVERFLOW_METHODS))){
			return $this::handle_invisible_method_call($method_name, $args);//call method handler for such scripts
		}
		if(LOGGING&&ALLOW_OVERFLOW_METHODS){//if error logging is enabled
			\shopping_agg\error_handle\error_log_c("Error: call to method $method_name, with args of ".var_export($args,true).", does not exsist.".((ALLOW_OVERFLOW_METHODS)?"\nGiven that ALLOW_OVERFLOW_METHODS is enabled looking through masters for that method":""));
			//deploy error and add and addendum if the constant ALLOW_OVERFLOW_METHODS is true
		}
		if(!ALLOW_OVERFLOW_METHODS){//if this method is not enabled block any further script execution
			\shopping_agg\error_handle\error_log_c("Error: call to method undefined method '$method_name'");//log error
			return false;//return standard error response
		}
		return $this::handle_invisible_method_call($method_name, $args);//call method handler for such scripts
		
	}
	/**
	 * The method as defiend for interactional classes. This allows for the automatic callng of methods of of parent classes without the need for refeincing a chain of masters.<br/>This will terminate at the first master that has that method defined.<br/>This should only be invoked by way of the __call() magic method and not called directly.<br/>  WARNING THIS METHOD USES EVAL HANDLE WITH CARE.
	 * @param string $method_name undefined method name
	 * @param array $args arguments passed to the undefiend method.
	 *
	 */
	public function handle_invisible_method_call($method_name,$args){
		if(method_exists($this, $method_name)){//check that method exsists
			/*
			return eval("return \$this->".$method_name."(".implode(",",array_map(//if so call function and get the return value using the eval statement
					function($arg){//pass anonymous functoin to the array map function call so that the arguments can be interpreded properly
						return var_export($arg,true);}//return an evaluatable variable value
						,$args)).
			");");
			*/
			$rm=new \ReflectionMethod(get_class($this)."::".$method_name);
			$rm->setAccessible(true);
			return $rm->invokeArgs($this,$args);
		}
		if($this->is_root_node()){//if master is not set
			\shopping_agg\error_handle\error_log_c("ERROR:Could not find the method in the interactional tree higherarchy.\nMethod undefined  across all masters of the object the method was called from. ", "call to $method_name, with the args of ".var_export($args,true));//log error about method failing to be found in upper level higherarchey
			return false;//return error down call chain
		}
		return $this->master->handle_invisible_method_call($method_name,$args);//jump up the tree to next master node to look for method.
	}
	/**
	 * This allows for passing of request to the base of the api tree for request processing
	 *	@param array $options This is an associative array of options for the request. The options are as follows are as follows: 
	 *	['endpoint'] => string $endpoint This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.(REQUIRED) 
	 *	['method'] => string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE") 
	 *	['post_data'] => string[] $post_data associative array, of single layer depth, that will be send as form parameters. 
	 *	['get_data'] => string[] $get_data associative array, of single layer depth, that will be send as a query string. 
	 *	['data'] => string[] $data associative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.) 
	 *	['api_key'] => string $api_key The name of the api_key to use (if API key not required don't define, if default is needed set to "DEFAULT") 
	 *  ['pre_request_function'] => string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
	 *  ['delay']=> int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
	 *  ['tries']=> int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1) 
	 *	Please refer to the API curl request handle documentation.
	 * @see api::curl_run
	 */
	public function request($options){
		if(!isset($this->master)){
			error_log("Error: malformed api key as request function could onr be found on a root node. passed request options are passed with this log",$options);
			return false;
		}
		return $this->master->request($options);
	}
	/**
	 * Validates if root the object this is called on is the root node of it's given tree
	 */
	protected function is_root_node(){
		if(isset($this->master)){//if ther is a master
			if (is_a($this, "shopping_agg\api\api")){//add exception as instances of api calsses should allways be rootnodes regardelsss of havin a master or nots
				return true;//this is a root node
			}
			return false;//this is not a root node
		};
		return true;//this is a root node 
		
	}
	/**
	 * This function checks the type of passed variable and attempts to convert it to a string where possible.
	 * If it is not successfully converted then the call will fail silently and return the error message as a string.(as well as logging a few errors)
	 * @param string $string The string to check the datatype of.
	 * @return string will contain an error message saying that the string conversion failed on error ("ERROR")
	 */
	protected function validate_string($string){
		if(!is_string($string)){
			
			\shopping_agg\error_handle\error_log_c("Error: argument paseed to a method of an instance of the ".get_class($this)." class as a ".gettype($string).". \n value:".var_export($string,true)."\n Trying to convert into string",$string);//log error of passed var not bieng a string
			
			try{
				$string=@(string)string;//attempt string conversion
			}
			catch(\Exception $e){
				$e_string="ERROR: \$string failed to convert to sting datatype. Please refer to logs(and enable LOGGING if not yet enabled)[Error thrown as ".$e->getMessage()." on line".$e->getLine()." in ".$e->getFile()."]";//save error message
				\shopping_agg\error_handle\error_log_c($e_string,$string);//log error with relavent problem variable
				$string="ERROR";//set string to be the error so that devs can find error locations and fix the issue.
				
			}
		}
		return $string;
	}
	//This verifies a passed uri
	/**
	 * This validates is a url (Uniform Resource Locator) is valid to be passed to the class
	 * @param string $url The uri to validate
	 * @param boolean $strict If the check should only focus on internal or external domains. If set to false file_exsists will also be used.
	 * @return string $url This will either be the passed url or the invalid URL on error
	 */
	protected function validate_url($url,$strict=true){
		if($this->validate_string($url)==="ERROR"){
			\shopping_agg\error_handle\error_log_c("Error: variable passed to a ".get_class($this)." object as an invalid url. (not a string)",$url);//log an error
			return INVALID_URL;
		}
		$url=trim($url," ");//trim whitespace for sanity checks
		if(filter_var($url, FILTER_VALIDATE_URL)===false&&$strict){//filter for a uri and if it is not one
			\shopping_agg\error_handle\error_log_c("Error: variable passed to a ".get_class($this)." object as an invalid url.",$url);//log an error
			$url=INVALID_URL;//and overwite the uri with the default invalid_uri
		}
		elseif(!$strict&&(filter_var($url, FILTER_VALIDATE_URL)===false)&&!file_exists($url)){//add type check for non url(s)
			\shopping_agg\error_handle\error_log_c("Error:  variable passed to a ".get_class($this)." object as an invalid url. (This includes it not bieng a valid local uri with an exsisting file.)",$url);
			$url=INVALID_URL;//and overwite the uri with the default invalid_uri
		}
		return $url;//return the uri
	}

	/**
	 * Validates a passed variable to see if it is a valid boolean.<br/> true,false,"true","false",1,0 are accepted where "true" and false are case insensative.
	 * @param mixed $bool The boolean value to validate
	 * @return boolean|int $bool The validated bool (0 on error)
	 * 
	 */
	protected function validate_bool($bool){
		if(is_numeric($bool)){// see if the boolean was passed as a number
			if(in_array(intval($bool),[0,1])){//is it 1 or 0
				$bool=(intval($bool)==1)?true:false;//if so convert that to the right boolean state and overide the original peramiter
			}
		}
		if(is_string($bool)){//if it is a string
			if(in_array(strtolower($bool),["true","false"])){// see if it is "true" or "false" (case insensative)
				$bool=(strtolower($bool)==="true")?true:false;// if so overwight the original peramiter with the correct value of a boolean
			}
		}
		if(!is_bool($bool)){//if it is not a boolean (agter all the checks it has been through)
			\shopping_agg\error_handle\error_log_c("Error: a non boolean datatype has been passed as an argument to an instance of the ".gettype($this)." class. Failed to convert to boolean and setting value to false",$bool);// log an error
			$bool=0;//set the variables state to false
		}
		return $bool;//finaly return the corrected boolean value (will not have change is boolean has passed) 
	}
	/**
	 * Validates and object type as an alias of the is_a function with a custom error logging functionality.
	 * @param mixed $var The variable to the a type check against
	 * @param string $type the type to check against NOTE: the full namespace of the type must be given with the type
	 * @param bool $log_error This argument defines if the method should log an error on a failure to match type.
	 * @return bool Returns either the true on a match or false on error;
	 * @see is_a
	 */
	protected function type_check($var,$type,$log_error=true){
		if(!is_a($var, $type)){//if it is not a class of this type
			if($log_error){
				\shopping_agg\error_handle\error_log_c("Error: Data passed to an instance of the ".get_class($this)." where the data shoud be a $type it is a ".get_class($var), $var);//log error
			}
			return false;//return error
		}
		return true;//return true
		
	}
	/**
	 * This handles type checks for master remapping so that master can be safley transitioned if needed.
	 * @param mixed $master the new master
	 * @param string $enforced_type the type to enforce if needed
	 * @return true on success and false on failure
	 */
	protected function remap_master_handle($master,$enforced_type=""){
		if($enforced_type!==""){//if there is an enforeced type
			if(strpos($enforced_type, "||")!==false){//look for or operator
				$enforced_type=explode("||", $enforced_type);//explode the array of valid masters
			}
			$enforced_type=is_array($enforced_type)?$enforced_type:array($enforced_type);//convert array
			foreach ($enforced_type as $type){//go through the array of possible types
				if($this->type_check($master, $type,false)){//da a check on that datatype
					$this->master=$master;//if the master is ok than remap it
					return true;// return succsess
				}
			}
			\shopping_agg\error_handle\error_log_c("Error: remapping of variable to new parent node failed due to invalid master type.\nTree structure most likley malformed.\nThis or the type of the passed object ".get_class($master)." is not any of thease types:\n".implode("\n",$enforced_type));//don't log in detail as that can be handled by the type_check error log.
			return false;//return false on error.
		}
		else{
			$this->master=$master;
			return true;
		}
	}
}
