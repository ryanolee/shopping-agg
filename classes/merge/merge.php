<?php
/**
 *
 */
namespace shopping_agg\merge;

/**
 * 
 * @author ryanolee
 * The abstract class for merge methods (inherits from \shopping_agg\api\interactional_class)
 */
abstract class merge extends \shopping_agg\api\interactinal_class{
	/**
	 * This function validates the response type of the aggragator.
	 * @param string $type The type of the data to merge. Must be "json,xml,jsonp" but is not strict.
	 * @return the normalized type e.g "javascript object notation"->"json"
	 */	
	protected function validate_response_type($type){
		if($type_new=parent::validate_string($type)==="ERROR"){
			\shopping_agg\error_handle\error_log_c("Error: ".gettype($type)." passed as a type to a merge object when it must be a string");
			return "UNKNOWN";
		}

		preg_match('/^(?Ji)(?:(?<json>json(?<jsonp>\\\\wp|wp|p)?|java(?:-|\s*)?script(?:\s*object\s*notation\s*(?<jsonp>(?:with)?\s*padding)?)?)|(?<xml>(?:x|ht)ml|(?:extended|hyper(?:-)?text)\s*mark(?:-|\s*)?up\s*language))$/i', $type, $matches);//worlds most forgiving type matching
		if(!empty($matches["jsonp"])){//given the way regex wis writtem give precedence to the jsop named capture group
			return "JSONP";
		}
		elseif(!empty($matches["json"])){
			return "JSON";
		}
		elseif(!empty($matches["xml"])){
			return "XML";
		}
		else{
			\shopping_agg\error_handle\error_log_c( "Error: Invalid response type passed to the merge object. ($type)");
			return "UNKNOWN";
		}
	}
	
	/**
	 * [DISABLED]
	 * This method verifies the format of the json selector to ensure it is a series of indexes. ["one"]["two"] t select {"one":{"two":{STUFF}}}
	 * @param string $data The secector to verify the format of.
	 * @return string $data Empty string on error and validated data otherwise.
	 */
	protected function validate_json_selector($data){
		return $data;//10/10 would validate again
		if(parent::validate_string($data)==="ERROR"){
			\shopping_agg\error_handle\error_log_c("Error: ".gettype($type)." passed as a type to a merge object as a selector when it must be a string");
			return "";
		}
		preg_match('/^(?:\s*\[\s*(?:"(?:[^"]|\\\\")*"|\'(?:[^\']|\\\\\')*\')\s*]\s*)*$/', $data, $matches);//worlds most forgiving type matching v2.0
		if(!empty($matches)){
			return $data;
		}
		\shopping_agg\error_handle\error_log_c("Error: JSON selector passed in an in an invalid format.Please use the format of \"['index1']['index2']\"", $data);
		return "";
	}
	/**
	 * This function validates if sub arrays in a 2d array are of regular lengths.
	 * @param string[][] $response The 2d array of retrieved objects
	 * @return bool true if all sub arrays are the same length;false otherwise
	 */
	protected function validate_lengths_of_retrieved_data($response){
		if(!is_array($response)){//if not array
			\shopping_agg\error_handle\error_log_c("Error: Unexpected ".gettype($response)." call made to merge::validate_lengths_of_retrieved_data without passing an array ", $response);
			return false;
		}
		if (count($response)==0){//if no data
			\shopping_agg\error_handle\error_log_c("Error: No data passed in array to method call merge::validate_lengths_of_retrieved_data.", $response);
			return false;
		}
		$valid=true;
		$last=count(current($response));
		foreach ($response as $key=>$data){
			if(count($data)!==$last){//if irregular collumn length
				\shopping_agg\error_handle\error_log_c("Warning: irregular length collumns. This may possibly result in unstable item normalisation. This may be due to an update in the api so please ensure that the selectors for ['$key'] are up to date.",$data);
				return false;
			}
			if(count($data)===0){
				\shopping_agg\error_handle\error_log_c("Error: var_to_look_for ['$key'] has resulted in finding no items.This may be due to an update in the api so please ensure that the selectors for ['$key'] are up to date and that your errors are all properly defined..",$data); 
				return false;
			}
		}
		return true;
	}
}
?>