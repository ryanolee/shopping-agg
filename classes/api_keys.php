<?php
/**
 * This document defines the api_keys class.
 * @author rizza <rizza@rizza.net>
 * @link api\api_keys
 * @package shopping_aggregator\interactional_layer\api_keys
 */

namespace shopping_agg\api;//define that this function is part of the api namespace

/**
 * 
 * @author rizza
 * @version 1.0
 * The master container for all required api_keys for any given api (in most cases this is only one api key but this is extendable to other api keys)
 * @property \shopping_agg\api\api_key[] $keys This is the asscocative ary inwitch key value pairs are stored. 
 */

class api_keys extends interactinal_class{// define new class (api_keys)
	private $keys;
	/**
	 * constructor method for array keys
	 * @param \shopping_agg\api\api $master REQUIRED the master node in the api tree to bind this to (should be the root node)
	 * @param array $keys array of api key objects OPTINAL there will need to be a defiend array of api_key objects for this to be passed as valid and will act as the initial store of access keys for the api
	 */
	public function __construct( $master,$keys=array()){
		//BEGIN CHECKS
		if(!is_a($master,"shopping_agg\api\api")&&$master!==null){//check that the master of this api_keys class instance of the api class
			if(LOGGING){// if this is not true and logging is enabled warn the user about the possible erronious concequences of using this class outside of context.  
				\shopping_agg\error_handle\error_log_c("WARNING: \$master passed to the api_keys object as something other than an instance of the api class.\n 
						This is not recomended and can lead to unpredictable class behaviour and fatal errors if not properly used in context.",$master);
			}
			$master=null;//set master to null in event of validation failure
		}
		$this->master=$master;//define master
		//commit checks for if $keys has been passed as an array of key objects or a key itself
		if(!is_array($keys)){//if the key is not an array
			if(is_a($keys,"shopping_agg\api\api_key")){//is it an api_key object?
				$keys=array($keys->get_name()=>$keys);//if so convert it into an array(and maintain value)
				//@todo Keep key consistency
			}
			else{//otherwise (if the key was not an array or not an api_key object)

				\shopping_agg\error_handle\error_log_c("ERROR: unable to define array keys as asscociative array variable transformed into empty array.\n
						Please ensure that the \$keys is passed as an array." ,$keys);

				$keys=array();//remove or ignore argument by setting it to its default state a
			}
			
		}
		foreach($keys as $key=>$value){//itterate through passed keys
			if(!is_a($value,"shopping_agg\api\api_key")){// if an item in the array is not an api key
				if(LOGGING){//log error where neccacery
					\shopping_agg\error_handle\error_log_c("Error: the api keys class is only supposed to be passed an array of api_keys an item has been passed as a ".gettype($key).".\nThis value key pair will be omitted.",array($key=>$value));//log error
				}
				unset($keys[$key]);//omit invalid array item
			}
			else{
				if(!$keys[$key]->remap_master($this)){
					\shopping_agg\error_handle\error_log_c("Error: failed to remap master of api_key object to api_keys object.\nThis value key pair will be omitted.",$keys[$key]);
					unset($keys[$key]);//omit invalid array item
				}
			}
		}
		//END CHECKS
		$this->keys=$keys;//safely set keys
	}
	/**
	 * This method remaps the master of the api_keys object to a new api class instance.
	 * @param shopping_agg\api\api $master The new api keys class instance to handle as master
	 */
	public function remap_master($master){
		return $this->remap_master_handle($master,"shopping_agg\api\api");
	}
	/**
	 * Add a new key to the api
	 * @param \shopping_agg\api\api_key new key to add.
	 * @return boolean false on error true on success
	 */
	public function set_key($value){
		$value=$this->check_api_keys($value);//Check api keys
		if ($value===false){//pass forward error
			return false;
		}
		$name=$value->get_name();//get the name
		$this->keys=array_merge($this->keys, array($name=>$value));//append key to prexsisting array using an associative binding
		return true;//return true for it being succsessfully added
	}
	/**
	 * Set the default key for any given api keys instance
	 * @param \shopping_agg\api\api_key $key
	 * @return boolean
	 */
	public function set_default($key){
		$key=$this->check_api_keys($key);
		if($key===false){//if an error occoured
			return false;//pass it forward
		}
		if(!$key->is_default()){//if the key is not a default
			$key->make_default();//make it so
		}
		return $this->set_key($key);//return the result of adding the key
	}
	/**
	 * Gets wether there is an exsisting default key for this set of api keys
	 * @return bool true if the api_keys instance has a default key and false otherwise.
	 */
	public function has_default(){
		return ($this->handle_default(false)===false)?false:true;//return false on not found of true otherwise using a standard ternary statement
	}
	/**
	 * This gets the default key from the api_keys instance
	 * @return \shopping_agg\api\api_key||bool (returns false on error)
	 */
	public function get_defualt(){
		return $this->handle_default();//call inturnal function for getting inturnal key
	}
	/**
	 * gets key with refrenced name
	 * @param string $key_name OPTINAL (in case of ther only being one stored key) The name of the key to get from the key store.
	 * @return \shopping_agg\api\api_key on succsuess NULL on error.
	 */
	public function get_key($key_name=""){
		//BEGIN CHECKS
		if(!is_string($key_name)){//if the key_name was passed as a non string
			\shopping_agg\error_handle\error_log_c("Error: call to api_keys::get_keys() had the key identifier(name) passed as a ".gettype($key_name)." where it should of been a string. NULL returned by default",$key_name);//log error with all relavent information
			return null;//return null as data would not of ever been found anyway as all identifiers are strings
		}
		//BEGIN SPECIAL USE CASES
		if($key_name===""&&count($this->keys)==1){//if there is only 1 key in the array and no paramiter passed return only item in array
			return $this->keys[0];//return the first item in the array
		}
		if($key_name==="DEFAULT"){//if the key required is the default key
			return $this->get_defualt();//return the default key
		}
		//END SPECIAL USE CASES
		if(!array_key_exists($key_name, $this->keys)){//if there is no key of that index in the array return null
			\shopping_agg\error_handle\error_log_c("Warning:key ['$key_name'] Not found in array keys index; returning null.",$key_name);//log error that key could not be found in array
			return null;//return null as no data can be found
		}
		//END CHECKS
		
		return $this->keys[$key_name];//return requested data value
		
	}
	/**
	 * an inturnal alias of the get_default function
	 * @param string $log_error wether to log an error or not
	 * @see \shopping_agg\api\api_keys::get_default()
	 */
	private function handle_default($log_error=true){
		if(count($this->keys)===1){//if there is only 1 key (assumption that given there is only one key it will be the default one)
			reset($this->keys);//reset array pointer
			return current($this->keys);//return first item of array
		}
		foreach($this->keys as $key){//for every key
			if($key->is_default()){//if it is default
				return $key;//return that key
			}
		}
		if($log_error){//if error logging is enabled (e.g this is not just a check)
			\shopping_agg\error_handle\error_log_c("Warning: Default key not found in call to api keys get default method.");//log that the key is not found
		}
		return false;//return error
	}
	/**
	 * Initiates tests for an api key
	 * @param \shopping_agg\api\api_key $value
	 * @return bool||\shopping_agg\api\api_key
	 */
	private function check_api_keys($value){
		//BEGIN CHECKS
		if(!is_a($value,"api_key")){//if $value is not an instance of the api_key class.
			if(LOGGING){//if logging is enabled
				\shopping_agg\error_handle\error_log_c("Error: passed \$value as a non api_key_instance",$value);	//log the error
			}
			return false;//return false for error
		}
		if($this->has_default()&&$value->is_default()){// if there is an prexsisting dafult that this is trying to overwrite
			\shopping_agg\error_handle\error_log_c("Warning: there was an attempted to pass a preexsisting api key as defult when one was allredy set.\nSetting as non default.", $value);//log error
			$value->default=false;//set values state as false
		}
		//END CHECKS
		return $value;//return value when properly filtered
	}
	
}