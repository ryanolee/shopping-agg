<?php
/**
 * This document defines the main interactional class
 * @author rizza <rizza@rizza.net>
 * @package shopping_aggregator\interactional_layer\interaction
 */

namespace shopping_agg\interaction;//define interactional namespace

use  api\interactinal_class as interactinal_class;//get interactional class and redefine


/**
 * 
 * @author ryanolee
 * This is the api interaction class for api objects where a representation of a curl request is held.
 * @property endpoint $endpoint The endpoint object bound to this interaction class instance.
 * @property response $response The response bound to this interaction class instance.
 * @property mixed $master the master for this object
 * @property error $error
 */
class interaction extends \shopping_agg\api\interactinal_class{
	/**
	 * 
	 * @param mixed $master The master of this class instance (not limited to any class)
	 * @param interaction\endpoint $endpoint This should be the related api endpoint class
	 * @param interaction\response $response 
	 * @param unknown $errors
	 */
	public function __construct($master,$endpoint,$response,$errors){
		$this->master=$master;
		$this->response=($this->type_check($response,"shopping_agg\interaction\\response"))?$response:new \shopping_agg\interaction\response($this,"");//use either the passed argument or a emptey one if the argument was of an incorrect datatype or not passed
		$this->endpoint=($this->type_check($endpoint,"shopping_agg\interaction\\endpoint"))?$endpoint:new \shopping_agg\interaction\endpoint($this);//use either the passed argument or a emptey one if the argument was of an incorrect datatype or not passed
		$this->endpoint->remap_master($this);//Update reference to this object
		$this->response->remap_master($this);//Update reference to this object	
	}
	/**
	 * Gets and returns the set api endpoint
	 * @return endpoint $endpoint
	 * @see \interaction\interaction::$endpoint
	 */
	public function get_endpoint(){
		return $this->endpoint;
	}
	/**
	 * This sets the state of the response variable
	 * @param \interaction\response $response The response object to update
	 */
	public function set_response($response){
		if(!$this->type_check($response,"\shopping_agg\interaction\response")){//if the passed var is not an instance of the responese class ...
			\shopping_agg\error_handle\error_log_c("Error: interaction::set_response called without passing a valid response object (state not updated).", $response);//log an error...
			return false;//and return false
		}	
		if(!$response->remap_master($this)){//try to remap master
			\shopping_agg\error_handle\error_log_c("Error: unable to set master in call to interaction::set_response.(state not updated) ", $response);
			return false;
		}
		$this->response=$response;//update state
		return true;// return successfull outcome
	}
	public function set_endpoint($endpoint){
		if(!$this->type_check($endpoint,"\\shopping_agg\\interaction\\endpoint")){//if the passed var is not an instance of the endpoint class ...
			\shopping_agg\error_handle\error_log_c("Error: interaction::set_endpoint called without passing a valid response object (state not updated).", $endpoint);//log an error...
			return false;//and return false
		}
		if(!$endpoint->remap_master($this)){//try to remap master
			\shopping_agg\error_handle\error_log_c("Error: unable to set master in call to interaction::set_response.(state not updated) ", $endpoint);
			return false;
		}
		$this->endpoint=$endpoint;//update state
		return true;// return successfull outcome
	}
	/**
	 * This method exexutes the cURL request by calling the request method and building the configuration array
	 * @param null|bool $handle_response if the response should be handled by the predefined request methods or not.
	 * @param bool $errors if errors should be logged or not.
	 * @return string $respone The responese  data of the cURL request
	 */
	public function exec_request($handle_response=true,$errors=false){
		if(is_a($this->master, "\shopping_agg\\api\\api")){//if the master of this is a root node
			$this->master->last[0]=$this->master->last[1];//move buffer into main
			$this->master->last[1]=array();//clear buffer
		}
		if(!$this->ready()){
			if($errors){
				\shopping_agg\error_handle\error_log_c("Error: call to exec_request failed as the ready state of the interactional layer is false.");
			}
			\shopping_agg\debug\debug_print("Call to make request to ".$this->endpoint->endpoint);
			
		}
		$response=$this->request($this->endpoint->build_configuration_array());
		if(!$response){
			return false;
		}
		if(is_a($this->master, "\shopping_agg\\api\\api")){//if the master of this is a root node
			if(is_null($this->master->next)){//if there is nothing to look for
				return $this->response->handle_response($response);//just return as normal
			}
			$this->master->set_next_ready_state_to_ready();
			$response=$this->response->handle_response($response);//handle the response specialy
			if(!is_array($response)){//handle array and assume $response is an array after this point
				$response_array=@json_decode(@json_encode($response),true);//if the data handled is not an associative array turn it into one
				if(!is_array($response_array)){//if convertion failed
					\shopping_agg\error_handle\error_log_c("Error: Failed to convert output of response object to array.Error occourred during json convertion [".json_last_error_msg()."]. Setting next url to be unready",$response,$response_array);
					$this->master->set_next_ready_state_to_unready();//block next
					return $response;//return given response
				}
				$response=$response_array;//set response array
				unset($response_array);//do a bit of cleanup
			}//look for matching key
			$matches=array();//http://stackoverflow.com/questions/2187629/can-i-get-all-keys-of-an-multi-level-associative-arrays-in-php
			foreach (new \RecursiveIteratorIterator(new \RecursiveArrayIterator($response), \RecursiveIteratorIterator::SELF_FIRST) as $k => $v) {
    			if ($k === DEFAULT_NEXT_URL_IDENTIFIER) {
        			$matches[] = $v;
    			}
			}
			$matchs_s=array();//flatten array
			foreach (new \RecursiveIteratorIterator(new \RecursiveArrayIterator($matches), \RecursiveIteratorIterator::SELF_FIRST) as $k => $v) {
				if(is_string($v)){//if the value is a string
					$matchs_s[]=$v;//welcome to the new world
				}
			}
			if(count($matchs_s)===0){//if no next url found
				//\shopping_agg\error_handle\error_log_c("Warning: Unable to find next url. setting ready state to unready for next item");
				//$this->master->set_next_ready_state_to_unready();//block next
				return $response;
			}
			$next_url=$matchs_s[0];
			//if($next_url=self::validate_string($matchs_s[0])==="ERROR"){
			//	\shopping_agg\error_handle\error_log_c("Error: Next url is not a valid string. setting ready state to unready for next item");
			//	$this->master->set_next_ready_state_to_unready();//block next
			//	return $response;
			//}
			\shopping_agg\debug\debug_print("Set next url to go to as [$next_url] .");
			$this->master->set_next_url($next_url);
			return $response;
		}
		return $this->response->handle_response($response); 
	}
	/**
	 * This function evaluates if the interaction clas is in a state that is redy to lunch the cURL request.
	 * @param boolean $errors Wether to launch errorsin the event of a false call or not.
	 * @return bool $ready (true on ready; false otherwise)
	 */
	public function ready($errors=false){
		return ($this->response->ready($errors)&&$this->endpoint->ready($errors));
	}
	/**
	 * This method is used to remap the master of interaction objects
	 * @param shopping_agg\api\api||shopping_agg\interaction\response $master The master to set this object to have
	 */
	public function remap_master($master){
		return $this->remap_master_handle($master,"shopping_agg\api\api||shopping_agg\interaction\\response");//r is a special charicter
	}
	
	
}