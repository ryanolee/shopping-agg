<?php
/**
 * 
 * This document defines the endpoint class.
 * @author rizza <rizza@rizza.net>
 * @link interaction\endpoint
 * @package shopping_aggregator\interactional_layer\interaction\endpoint
 */

namespace shopping_agg\interaction;//define interactional namespace

use api\interactinal_class as interactinal_class;//get class
use shopping_agg;
use function shopping_agg\error_handle\error_log_c;
/**
 * 
 * @author ryanolee
 * The endpoint class is holds all data neccecery for prerequest cURL execution.
 * @property intercation\interaction $master the master for any instance endpoint class should only ever be the related ineraction class instance. 
 * @property string $endpoint This is the property that stores the URL that the endpoint will be ending it's request out to. This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.
 * @property string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE")
 * @property string[] $post_data assoiative array, of single layer depth, that will be send as form peramiters.
 * @property string[] $get_data assoiative array, of single layer depth, that will be send as a querey string.
 * @property string[] $data assoiative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.)
 * @property string $api_key The name of the api_key to use (if api key not required dont define, if default is needed set to "DEFAULT")
 * @property string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
 * @property int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
 * @property int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1) 
 */
class endpoint extends \shopping_agg\api\interactinal_class{
	/**
	 * @var array $handle_later This array holds queued items during the need for inturnal variable processing.
	 */
	private $handle_later;
	/**
	* Constructor method for the endpoint class.
	* All permiters are optional but must be defiend by the time that the request execute method is called on it or requests sent from it will scilently fail.
	* @param intercation\interaction $master the master for any instance endpoint class should only ever be the related ineraction class instance. 
	* @param string $endpoint This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.
	* @param string $method The method that the request should be sent as.(must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE")
	* @param string[] $post_data assoiative array, of single layer depth, that will be send as form peramiters.
	* @param string[] $get_data assoiative array, of single layer depth, that will be send as a querey string.
	* @param string[] $data assoiative array, of single layer depth, that will be send over the default defined method (GET if defined method does not have a send type.)
	* @param string $api_key The name of the api_key to use (if api key not required dont define, if default is needed set to "DEFAULT")
	* @param string $pre_request_function This is the function to call in the case calling before the final request is sent (in the same format as special vars).
	* @param int $delay The delay (in seconds) to wait before launching the request to circimvent throttling from api(s). (default 0)
	* @param int $tries The number of times to attempt to connect in total in the event of a failed connection (default 1) 
	*/
	public function __construct($master=null, $endpoint="", $method="GET", $post_data=array(), $get_data=array(), $data=array(),$api_key="",$pre_request_function="",$delay=0,$tries=1){
		$this->endpoint=(strpos($endpoint,"{")===false)?$endpoint:$this->validate_url($endpoint);//assign api endpoint (is there is a special var in the url assume it is correct and deal with it later)
		$this->post_data=$this->check_data_array($post_data);//endpoint
		$this->data=$this->check_data_array($data);//validate data arrays
		$this->get_data=$this->check_data_array($get_data);
		$this->method=$this->check_method_type($method);
		$this->pre_request_function=$pre_request_function;//FIXME
		$this->master=$master;
		$this->api_key=self::validate_string($api_key);
		$this->handle_later=array();
		$this->delay=$this->validate_delay($delay);
		$this->tries=$this->validate_tries($tries);
	}
	/**
	 * This method is used to remap the master of endpoint objects
	 * @param \shopping_agg\interaction\interaction $master
	 */
	public function remap_master($master){
		return $this->remap_master_handle($master,"\shopping_agg\interaction\interaction");//escape special charicter
	}
	/**
	 * This fuction builds and returns the configuration options data stored in this object instance
	 * @return string[] $options The array of configuration options to follow.
	 */
	public function build_configuration_array(){
		if(DEBUGGING){
			$before=microtime(true);
		}
		$data=array("data"=>&$this->data,//construct data array
					"get_data"=>&$this->get_data,
					"post_data"=>&$this->post_data
					);
		foreach ($data as $data_key=>$data_value){//special value parseing
			$data[$data_key]=$this->get_special_vars($data_value,$data_key);
		}
		$this->handle_required_later();
		if($this->pre_request_function!==""){//create pre request function
			$this->pre_request_function=$this->parse_special_var($this->pre_request_function,"",false,array(),"",false);//resolve W\O first layer
		}
		$this->handle_required_later();//re-resolve required later
		$data=array("data"=>$this->data,//construct data array
					"get_data"=>$this->get_data,
					"post_data"=>$this->post_data
					);//reasign data
		if(isset($data["data"]["data_from_response"])){//if there is lingering response data
			unset($data["data"]["data_from_response"]);//pruge it
		}
		$this->handle_url();
		if(DEBUGGING){
			$after=microtime(true)-$before;
			\shopping_agg\debug\debug_print("Total time taken to genarate request: $after");
		}
		return array_merge($data,array(
					"method"=>$this->method,
					"api_key"=>$this->api_key,
					"endpoint"=>$this->endpoint,
					"pre_request_function"=>$this->pre_request_function,
					"delay"=>$this->delay,
					"tries"=>$this->tries
				));//Get all coniguration data into its required format(Combine data array with normal options array)
		
			
				
	}
	/**
	 * set the endpoint for the endpoint class instance.
	 * @param string $endpoint the url to set as the endpoint (This MUST BE THE FULL URL(https://blablabla.com/xy/z) and not an absolute (/xy/z) or relative (./xy/z) linking.)
	 */
	public function set_endpoint($endpoint){
		$this->endpoint=$this->validate_url($endpoint);
	}
	/**
	 * This checks the state of the endpoint object and evaluates if it is in a reasonable state to send off with. 
	 * @param bool $errors If to display log error on calls or not (will log error on $errors being true)(false otherwise)
	 * @return bool $ready Returns true on the class bieng ready and false otherwise.
	 */
	public function ready($errors=false){//@todo add more eval criterium.
		if($this->endpoint===INVALID_URL){
			if($errors){
				\shopping_agg\error_handle\error_log_c("Warning: The state of the endpoint is equal to that of the INVALID_URL constant. This could signify the calling of the function in an invalid state. However, given that the fallback uri may be valid, returning readystate of true.",$this->endpoint);
			}
		}
		return true;
	}
	/**
	 * This function handles and parses the url for special variable componants for the next phase of url encoding {special_var}->special_var_value
	 */
	private function handle_url(){
		//$endpoint_temp=
		preg_match_all("/(?:{([^}]+)})+/U",$this->endpoint , $matches);//get the special vars
		if(!self::empty_recursive($matches)){
			$key=$matches[0];//se out things to replace
			$value=$matches[1];//and the temp values we are going to replace them with
			foreach($value as &$val){
				$val=$this->parse_special_var($val,"",true);
			}
			foreach(array_combine($key,$value) as $key=>$value){
				if($key=="{BASE_URL}"){
					if(($focus=$this->get_root_node()->search->endpoint)===$this){//if we are looking for ourself
						\shopping_agg\error_handle\error_log_c("Warning: you cannot use the {BASE_URL} special variable on the root url as curcular references can arise from it.");
						$value="";
					}
					else {
						$url=parse_url($focus->endpoint_orig);
						$value=$url["scheme"].$url["host"].$url["path"];//I like to live on the edge ()
					}
				}
				 $this->endpoint=str_replace($key, $value, $this->endpoint);
			}
			
		};
		$this->endpoint=$this->validate_url($this->endpoint,true);
	}
	/**
	 * This function validates the delay property of class instances
	 * @param int $delay the delay to validate
	 * @param bool $log_errors If errors should be logged or not
	 * @return int The validated int or 0 on fail
	 */
	private function validate_delay($delay,$log_errors=true){
		if(is_int($delay)){//if is int
			return $delay;//return immeietly
		}
		if(is_float($delay)){//if float
			if($log_errors){
				\shopping_agg\error_handle\error_log_c("Warning: delay passed as float when it should be an int (applying ceiling function)");
			}
			return ceil($delay);//round up
		}
		if($log_errors){
			\shopping_agg\error_handle\error_log_c("Error: delay passed as non numeric value (defaulting to 0)");
		}
		return 0;//otehrsise return 0
	}
	/**
	 * This function validates the number of times a request should be attempted
	 * @param int $tries the variable to validate.
	 * @return int $tries the validated tries variable (1 on fail)
	 */
	private function validate_tries($tries){
		$tries=$this->validate_delay($tries,false);//do int validaton
		if($tries<1){
			\shopping_agg\error_handle\error_log_c("Error: number of tries passed < 1... returning 1");
			return 1;
		}
		return $tries;
	}
	/**
	 * This function handles the parsing of special variable values ("POST:'post index'"-> the value of $_POST['post index']) and converts them into there corect values(same with get: and function: special operators)
	 * @param array $data The data array to look through for special values.
	 * @param string $datastore The name of the data_array that is currnently bieng iterated through (get_data/post_data/data)
	 * @return array $data the updated array with new values.
	 */
	
	private function get_special_vars($data,$datastore=""){
		foreach($data as $key=>$value){//for each item in parsable array
			$data[$key]=$this->parse_special_var($value,$key,false,array(),$datastore);
		}
		return $data;
	}
	/**
	 * This function gets the highest value,for a precedence, from "handle_later" data set.
	 * @return int $default The default precedent of the current "handle_later" data set.
	 */
	private function get_default_precedence(){
		if(count($this->handle_later)===0){//if there is no queued item set its importance to default (0).
			return 0;
		}
		$largest_val=0;
		foreach ($this->handle_later as $queued_item){//for everey item in the queue
			if($largest_val<$queued_item["precedence"]){//get the lowest precedence
				$largest_val=$queued_item["precedence"];
			}
		}
		return $largest_val;
	}
	/**
	 * This method updates the default precedences to the highest value in the given set of defined data references 
	 */
	private function update_precedence(){
		$precedence=$this->get_default_precedence();
		$this->handle_later= array_map(function ($item)use ($precedence){
			if($item["automatic"]){
				$item["precedence"]=$precedence;
				return $item;
			}
			return $item;
		},$this->handle_later);
	}
	/**
	 * This functions uses a bubble sort to reorder the items in order of acending precedence
	 * @return bool True on success false otherwise
	 */
	private function order_handle_later_values(){
		if(count($this->handle_later)<=1){
			return true;
		}
		$ordered=false;
		$ordered=false;
		while(!$ordered){//use a bubble sort
			
			$ordered=true;
			foreach (range(0,count($this->handle_later)-2) as $key){
				if($this->handle_later[$key]['precedence']>$this->handle_later[$key+1]['precedence']){//swap items if the item at [x]>[x+1]
					$back=$this->handle_later[$key];
					$this->handle_later[$key]=$this->handle_later[$key+1];
					$this->handle_later[$key+1]=$back;
					$ordered=false;//not ordred
				}
			} 
		}
		return true;
	}
	/**
	 * This function handles the getting of special values after the non dependant set of required variable have been resolved.
	 */
	private function handle_required_later(){
		$this->order_handle_later_values();
		while(!empty($this->handle_later)){//while the array has not been exsausted(not done in foreach loop as state of array modified duing each iteration)
			$object_to_resolve=$this->handle_later[0];//get first item
			switch($object_to_resolve["orig_data_store"]){//select correct data location (assign by refrence)
				case "data":$data=&$this->data;break;
				case "post_data":$data=&$this->post_data;break;
				case "get_data":$data=&$this->get_data;break;
				default:$data=&$this->data;break;
			}
			$data[$object_to_resolve["orig_key"]]=$this->parse_special_var($object_to_resolve["value"],$object_to_resolve["key"],true);//update reference
			array_shift($this->handle_later);//remove item from array after resolving
		}
	}
	/**
	 * 
	 * This function handles the parsing of special variable values ("POST:'post index'"-> the value of $_POST['post index']) and converts them into there corect values(same with get: and function: special operators)
	 * @param string var The variable to parse. Please note that this is a recoursive callback and can handle nested functions. However this is not true for get or post special variables.
	 * @param string $key the key of the object to obtain a marker from.
	 * @param boolean $$recursive_callback if the callback to the function should be considered a recoursive call (one where a the inturnal state of the script is bieng parsed)
	 * @param array $stack the stack of currently resolving data objects (not function calls)
	 * @param string $datastore The original store of data that the variable was called from
	 * @param bool $resolve_first_function_call This bollean signifies if the first function call should be made or not (Not required most of the time.)
	 * @return string The function should only result in a string.
	 * 
	 */
	private function parse_special_var($var,$key="",$recursive_callback=false,$stack=array(),$datastore="",$resolve_first_function_call=true){
		//PREEXSISTING VARIABLES
		if(!$recursive_callback){//if this is an initial call
			preg_match('/.*\s*(?<data_loc>(get_|post_)?data)\s*:\s*\'(?<name>[^\']+)\'\s*(?:precedence\s*:\s*\'\s*(?<data_precedence>\d{1,5})\s*\'\s*)?/i', $var,$match);
			if(!self::empty_recursive($match)){//if call is made for preexsisting var
				if(isset($match["data_precedence"])){
					array_push($this->handle_later, array("data_location"=>$match["data_loc"],"value"=>$var,"key"=>$match["name"],"precedence"=>(int)$match["data_precedence"],"automatic"=>false,"orig_key"=>$key,"orig_data_store"=>$datastore));
					$this->update_precedence();
				}
				else{
					array_push($this->handle_later, array("data_location"=>$match["data_loc"],"value"=>$var,"key"=>$match["name"],"precedence"=>(int)$this->get_default_precedence(),"automatic"=>true,"orig_key"=>$key,"orig_data_store"=>$datastore));
				}
				return $var;//return value
			}
		}
		else{//otherwise
			preg_match('/^\s*(?<data_loc>(get_|post_)?data)\s*:\s*\'(?<name>[^\']+)\'\s*(?:precedence\s*:\s*\'\s*(?<data_precedence>\d{1,5})\s*\'\s*)?$/i', $var,$match);//check for perfect match
			if(!self::empty_recursive($match)){//if call is made for preexsisting var
				switch($match["data_loc"]){//select correct data location (assign by refrence)
					case "data":$data=&$this->data;break;
					case "post_data":$data=&$this->post_data;break;
					case "get_data":$data=&$this->get_data;break;
					default:$data=&$this->data;break;
				}
				foreach ($stack as $item){//check for circular refrences e.g data:{"hello":"data:'hello'"}
					if($item['data_location']===$match["data_loc"]&&$match["name"]===$item['key']){//you
						\shopping_agg\error_handle\error_log_c("Error: Unable to resolve refrence to '".$match["name"]."' in made in '".$key."' given that it is makes a circular refrence to itself!");					
						//$data[$match["key"]]="";//set to null
						return "";
					}
				}
				foreach ($this->handle_later as $item){
					if($item['data_location']===$match["data_loc"]&&$match["name"]===$item['key']){//if the varible is still waitning to be processed processed
						$data[$match["name"]]=$this->parse_special_var($data[$match["name"]],$match["name"],true,array_merge($stack,array(array("data_location"=>$match["data_loc"],"key"=>$match["name"]))));//process the required data first
						foreach ($this->handle_later as $data_key=>$item){
							if($item["orig_key"]==$key){
								$to_unset=$data_key;
								break;
							}
						}
						
						if(@isset($this->handle_later[$to_unset])){//if the key was found
							unset($this->handle_later[$to_unset]);//after processing terminate that item
						}
						$this->handle_later=array_values($this->handle_later);//rest array orderings
						$data[$match["name"]];//return the newly processed data update reference
						return $data[$match["name"]];
					}
				}
				 $data[$key]= $data[$match["name"]];//update reference
				 return $data[$match["name"]];
			}
		}
		//POST
		preg_match("/^\s*POST\s*:\s*(?:'(?<var_name>[^']*)'|\"(?<var_name_2>[^\"]*)\")\s*$/si", $var,$match);//  [/^\s*POST\s*:\s*(?:'(?<var_name>[^']*)'|"(?<var_name_2>[^"]*)")\s*$/gsi] //regex with no escape charicters
		if(!self::empty_recursive($match)){
			//var_dump($match);
			$match=(isset($match["var_name"]))?$match["var_name"]:$match["var_name_2"];//select correct named capture group
			if(!isset($_POST[$match])){//if the match failed
				\shopping_agg\error_handle\error_log_c("Warning: An expected post variable (\$_POST[$match]) does not exsist or was never passed. This was defined for the data that should of been passed forwards to the api. Setting data to empty string and skipping item.", $match);//log error
				$var="";
				return $var;//return value
			}
			else{
				$var=$_POST[$match];
				return $var;//return value
			}
		}
		//GET @todo possible to optimize
		preg_match("/^\s*GET\s*:\s*(?:'(?<var_name>[^']*)'|\"(?<var_name_2>[^\"]*)\")\s*$/si", $var,$match);
		//var_dump($match);
		if(!self::empty_recursive($match)){
			//$match=$match[0];//take first match and set that as the main selected value
			$match=(isset($match["var_name"]))?$match["var_name"]:$match["var_name_2"];//select correct named capture group
			if(!isset($_GET[$match])){//if the match failed
				\shopping_agg\error_handle\error_log_c("Warning: An expected get variable (\$_GET['$match']) does not exsist or was never passed. This was defined for the data that should of been passed forwards to the api. Setting data to empty string and skipping item.", $match);//log error
				$var="";
				return $var;//return value
			}
			else{
				$var=$_GET[$match];
				return $var;//return value
			}
		}
		//GET
		//LAST
		preg_match('/^\s*last\s*:(?:(?<last_index>\d{1,4}):)?(?<datatype>get_data|post_data|data)\s*(?:\'(?<var_name>[^\']*)\'|\"(?<var_name_2>[^\"]*)")\s*$/',$var,$match);
		if(!self::empty_recursive($match)){
			$index=isset($match["last_index"])?$match["last_index"]:0;//get index
			$last=$this->get_root_node();
			//var_dump($last);
			$last=$last->get_last_data($index);//get the last item
			$var_name=(isset($match["var_name"]))?$match["var_name"]:$match["var_name_2"];
			if(!$last){
				\shopping_agg\error_handle\error_log_c("Error: unable to find last item at index [$index].");
				return "";
			}
			if(!isset($last[$match["datatype"]][$var_name])){//if data does not exsist
				\shopping_agg\error_handle\error_log_c("Error: unable to find last item at index [".$match["datatype"]."][".$var_name."]");
				return "";
			}
			//var_dump($last[$match["datatype"]][$var_name]);
			return $last[$match["datatype"]][$var_name];
		}
		//END LAST
		//FUNCTION CALLS
		//matches "function:'func_name(args)'" or "function:'funcname'args:'args'" where the prior is preferenced
		preg_match('/^\s*function\s*:\s*\'(?<funcname>[a-zA-Z0-9_\x7f-\xff]*+)(?:\((?<args_direct>(?:.*)*)\))?\'\s*(?:args\s*:\s*\'(?<args>.)\')?\s*$/i', $var,$match);
		if(!self::empty_recursive($match)){//if there was an match
			//@ob_start();//block output from eval
			if(isset($match["args_direct"])){//prefrence args direct
				$match["args"]=$match["args_direct"];
			}
			if(!isset($match["args"])){//if neiter are set call using the first available one
				$match["args"]="";
				$match["args_direct"]="";
			}
			$args=explode(",",$match["args"]);
			if (count($args)!=0){//if there are no arguments
				$args=array_map(//get args
						function($data)use($key,$stack){ //for each argument
								if(strlen($data)==0){//dont bother non exsistant arguments.
									return "";
								}
								return var_export(shopping_agg\interaction\endpoint::parse_special_var($data,$key,true,$stack),true);//parse each special var
						},$args);
			}
			if(!$recursive_callback&&!$resolve_first_function_call){//If you know you should not be making the function call and it is the first layer dont make it.
				$var="\\shopping_agg\\safe_func\\".$match["funcname"]."[Inturnal_break]".implode(",", array_map(function($data){return trim($data,"'");}, $args));//set up for later full calls
			}
			else{
				$var=call_user_func_array("\\shopping_agg\\safe_func\\".$match["funcname"],$args);
			}
			if($var===false||$var===null){//if an error occourred
				\shopping_agg\error_handle\error_log_c("Error: Call to function shopping_agg\\safe_func\\".$match["funcname"]."failed with passed args ".$match["args"]." due to unknown reasons. (or function returned false)", $match["funcname"],$match["args"]);
				return $var;
			}
			@$var=(string)$var;//try to convert
			if(!is_string($var)){// if convertion fails
				\shopping_agg\error_handle\error_log_c("Error:unable to to convert value of function call to string for ['$key']. Reverting to previous value. Setting variable to empty string.",$var);//log error
				return $var;//return value
			}
			//@ob_end_clean();//reenable it
		}
		return $var;//return value
	}
	/**
	 *@ignore
	 *Only for inturnal functioning of get special vars.
	 *
	 */
	static private function empty_recursive($value)
	{
		if (is_array($value)) {
			$empty = TRUE;
			array_walk_recursive($value, function($item) use (&$empty) {
				$empty = $empty && empty($item);
			});
		} else {
			$empty = empty($value);
		}
		return $empty;
	}
	
	/**
	 * This checks an array of data for if it is a valid format or not.
	 * @param string||array $data_array This either needs to be an associative array or an JSON string with a single layer of depth.<br/>The associative array should contain key value pairs that are both strings.
	 * @return array $data_array An array of data of a valid type.
	 */
	private function check_data_array($data_array){
		if(!is_array($data_array)&&is_string($data_array)){//if data is not an array and a string
			$data_array_decoded=json_decode($data_array,true,2);//check that it is a string and try to decode it into json if possible (allowing for 1 layer depth)
			if(is_null($data_array_decoded)||$data_array_decoded===false){//if convertion failed
				\shopping_agg\error_handle\error_log_c("Error: Failed to decode passed string JSON string into associative array.\nPlease ensure that the json is valid and 1 layer deep.\nReturning emptey array.", $data_array_decoded);//log error
				return array();
			}
			$data_array_decoded=$data_array;//overwite old string with coverted array
		}
		if(!is_array($data_array)){//if it is still not an array (IE convertion failed)
			\shopping_agg\error_handle\error_log_c("Error: An invalid datatype has been passed as data to an api endpoint.\nReturning emptey array.",$data_array);//throw error if invalid datatype
			return array();
		}
		
		$numeric_key_found=false;//look for numeric keys
		foreach ($data_array as $key=>$value){
			if(is_numeric($key)){//if a key is numeric
				$numeric_key_found=true;//declare that a numeric key has been found
			}
			if(is_array($value)){
				\shopping_agg\error_handle\error_log_c("Error: The data passed to the api endpoint object should only be 1 layer in depth ommitting from data array $key",$data_array,array($key=>$value));//log error
				unset($data_array[$key]);//ommit invalid item.
				continue;//restart array iteration
			}
			if(!is_string($value)){
				try{
					$data_array[$key]="$value";
				}
				catch (Exception $e ){
					\shopping_agg\error_handle\error_log_c("Error: Unable to convert item in array to a string datatype.\n omitting result.",$data_array,$data_array[$key]);
					unset($data_array[$key]);//ommit invalid item.
					continue;//restart array iteration
				}
			}	
		}
		if($numeric_key_found){
			\shopping_agg\error_handle\error_log_c("Warning: the  data array passed to an endpoint object contains numeric indexes.\n This could mean that it is not an associative array and may result in api call errors!", $data_array);
		}
		return $data_array;
	}
	/**
	 * Validation for the method type of an request object
	 * @param string $request_type must be "GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS" or "TRACE"
	 * @return either the value of INVALID_REQUEST_METHOD on failure or the original variable passes (in upper case)
	 * @see INVALID_REQUEST_METHOD
	 */
	private function check_method_type($request_type){
		if(!is_string($request_type)){// if the metod is not a string
			\shopping_agg\error_handle\error_log_c("Error: invalid request method as it was not passed as a string (defaulting to ".INVALID_REQUEST_METHOD.").", $request_type);//say so
			return INVALID_REQUEST_METHOD;//return error type
		}
		if(!in_array(strtoupper($request_type), ["GET","HEAD","POST","PUT","DELETE","CONNECT","OPTIONS","TRACE"])){//if not valid request method
			\shopping_agg\error_handle\error_log_c("Error: invalid request method passed to an endpoint object.", $request_type);//throw error
			return INVALID_REQUEST_METHOD;
		}		
		return strtoupper($request_type);
	}
}