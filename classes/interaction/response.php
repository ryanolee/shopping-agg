<?php
/**
 * This document defines the response class as part of the interactional tree
 * @author rizza<rizza@rizza.net>
 * @package shopping_aggregator\interactional_layer\interaction\response
 */
namespace shopping_agg\interaction;//define interactional namespace




use shopping_agg;
use function shopping_agg\debug\debug_print;

//use  api\interactinal_class as interactinal_class;//get interactional class and redefine
/**
 *
 * @author rizza <rizza@rizza.net>
 * 
 * This is the response data holder for response object 
 * @see shopping_agg\merge\merge
 * @property string $pivot The pivot leading to the array containing items
 * @property string $response_type The expected document resonse type ["JSON","XML" or "JSONP"]
 * @property string[] $resource queue An inturnal queue of specialy formatted array items that come in the form array(["goto"]=>interactional_class_instance,["selector"]=>link_to_url)
 */
class response extends \shopping_agg\merge\merge{
	/**
	 * Constructor method for response class.
	 * @param string $response_type The expected document resonse type ["JSON","XML" or "JSONP"]
	 * @param string $pivot The pivot leading to the array containing items
	 * @param array $vars_to_look_for The variables to look for
	 * @param \shopping_agg\interaction\interaction $master The master to assign to this response class instance
	 */
	public function __construct($response_type="JSON",$pivot="",$vars_to_look_for=array(),$master=NULL){
		$this->response_type=$this->validate_response_type($response_type);
		if($this->response_type==="JSON"||$this->response_type==="JSONP"){
			$this->pivot=$this->validate_json_selector($pivot);
		}
		else{
			$this->pivot=$pivot;
		}
		$this->vars_to_look_for=$this->validate_vars_to_look_for($vars_to_look_for);
		$this->resource_queue=array();
		$this->master=$this->type_check($master, "\shopping_agg\interaction\interactional_class",false);
		
	}
	/**
	 * This method is used to remap the master of endpoint objects
	 * @param \shopping_agg\interaction\interaction||\shopping_agg\interaction\\response $master
	 */
	public function remap_master($master){
		return $this->remap_master_handle($master,"\shopping_agg\interaction\interaction||\shopping_agg\interaction\\response");
	}
	/**
	 * This method is set to handle the response of an api request
	 * @param string|stdClass|array $data The data to handle the response of
	 * @return mixed the handled data (false on error)
	 */
	public function handle_response($data){//FIXME
		if(DEBUGGING){
			$before=microtime(true);
		}
		if($this->response_type=="JSON"){
			debug_print("Converting json response to xml");
			$json=$data;
			if(is_string($json)){
				$json=\shopping_agg\parse\jsonp_decode($data,true);//decode the json string
			}
			elseif(is_a($json, "\stdClass")){
				$json=json_decode(json_encode($data),true);//convert json to an array
			}
			if(!$json){
				\shopping_agg\error_handle\error_log_c("Error: failed to parse json. \n Invalid syntax. Error returned as:\n ".var_export(json_last_error_msg(),true));//log error
				return false;
			}
			$xml = new \SimpleXMLElement('<template></template>');
			\shopping_agg\parse\array_to_xml($json,$xml);
			$data=$xml->asXML();
			
		}
		try{
			$response_data=($this->response_type==="XML")?$this->get_vars_from_xml_response($data):$this->get_vars_from_xml_response($data);//$this->get_json_vars_from_response($data); just use xml lol
		}
		catch(\Exception $e){//cathc exceptions
			\shopping_agg\error_handle\error_log_c("Error: an Exception was thrown on line ".(string)$e->getLine()." in ".$e->getFile()." .\n Thhis error as a result of trying to get data from raw api output Error thrown:'".$e->getMessage()."'");
			$response_data=$data;//settle for raw data output.
		}
		catch(\Error $e){//catch errors
			\shopping_agg\error_handle\error_log_c("Warning: an Exception was thrown on line ".(string)$e->getLine()." in ".$e->getFile()." .\n Thhis error as a result of trying to get data from raw api output Error thrown:'".$e->getMessage()."'");
		}
		if(DEBUGGING){
			$after=microtime(true)-$before;
			\shopping_agg\debug\debug_print("Total time taken to handle response: $after");
		}
		$extra_response_data=$this->get_data_from_exturnal_resources();//pull in extra data
		if(STRICT_ITEM_COUNT){
			if(!$this->validate_lengths_of_retrieved_data($response_data)){
				\shopping_agg\error_handle\error_log_c("Error: STRICT ITEM COUNT set to true but irregular item count given.",$this->vars_to_look_for);
				return false;
			}
		}
		if(!empty($extra_response_data)){//if data needs to be merged
			$response_data=array_replace($response_data, $extra_response_data);
		}
		return $response_data;
		
	}
	/**
	 * This checks the state of the response object and evaluates if it is in a reasonable state to send off with.
	 * @param bool $errors If to display log error on calls or not (will log error on $errors being true)(false otherwise)
	 * @return bool $ready Returns true on the class bieng ready and false otherwise.
	 */
	public function ready($errors=false){
		if($this->response_type=="UNKNOWN"){
			if($errors){
				\shopping_agg\error_handle\error_log_c("Error: the response type is unknown so request is not ready");
			}
			return false;
		}
		return true;
	}
	/**
	 * This object handles object normalization and its behaviour depends on the NORMALIZE_API_OBJECT constant defined in config.php (see NORMALIZE_API_OBJECT for validation options)
	 * @see  \NORMALIZE_API_OBJECT
	 */
	private function handle_object_normalization(){
		//too many loops x_x
		if(NORMALIZE_API_OBJECTS===0){
			return true;
		}
		if(NORMALIZE_API_OBJECTS===11||NORMALIZE_API_OBJECTS===12){
			if(!$this->validate_lengths_of_retrieved_data($this->vars_to_look_for)){
				error_log_c("Error: Ireggular number of items passed to during object normalization when strict normalization was in effect. (Check your selectors)");
				return false;
			}
		}
		$iterator= new MultipleIterator ();
		$vars_to_normalize=[];
		$vars_to_convert=[];
		$final_data=[];
		foreach($vars_to_look_for as $key=>$var){//sort lists
			if(isset($var["goto"])&&isset($var["selector"])){//if the thing itself needs to be converted
				$vars_to_convert=array_replace($vars_to_convert, array($key=>$var));//push it on the convertion queue
			}
			else{
				$vars_to_normalize=array_replace($vars_to_normalize, array($key=>$var));//push it on the convertion queue;
			}
		}
		$vars_to_normalize_keys=array_keys($vars_to_normalize);//save the key
		if(NORMALIZE_API_OBJECTS===21||NORMALIZE_API_OBJECTS===22){//if the length is arbitrerey
			$largest=0;
			foreach ($vars_to_normalize as $var){
				if(count($var)>$largest){
					$largest=count($var);//get largest array
				}
			}
			array_walk($vars_to_normalize,function(&$item) use ($largest){//walk the array
				$item=array_merge($item,
						($largest===count($item))?//if the item is == to count
						array()://do nothing
						array_map(function($item){return "";},range(0,$largest-count($item))));//fill the array with empty data where needed
			});
		}
		
		foreach ($vars_to_normalize as $var){//YOO DAWG I HEAD YOU LIKE ITERATORS SO I MADE AN ITERATOR TO ATTACH ITORATORS TO YOUR ITERATORS SO YOU CAN ITERATE WHILE YOU ITERATE.
			$iterator->attachIterator (new ArrayIterator ($var));//bind all iterators as required
		}
		foreach ($iterator as $item){
			$final_item=[];
			foreach (range(0,count($item)-1) as $x){//needs testing
				$final_item=array_replace($final_item, array($vars_to_normalize_keys[$x]=>$item[$x]));
			}
			$final_item[]=$final_item;
		}
		array_walk($vars_to_convert,//convert layers above
					function(&$item){
						$data=$item["goto"]->response->handle_object_normalisation();//do it recoursivly
						$item=(is_array($data)||$data instanceof \stdClass)?$data:array();//if invalid datatype set to empty array
					}
				);
		$final_data=array_merge($final_data,$vars_to_convert);
		if(NORMALIZE_API_OBJECTS===22 || NORMALIZE_API_OBJECTS===12){
			$final_data=(object)$final_data;//if data needs to be normalized into an object do so
		}
		return $final_data;//finaly return the data
		
	}
	/**
	 * This function returns previously expected results that required going to exturnal url(s) 
	 * This method should be called after getting all the required results for that function
	 */
	private function get_data_from_exturnal_resources(){
		if(!isset($this->resource_queue)){
			return false;
		}
		$final_return_data=array();
		foreach ($this->resource_queue as $var_index=>$selector_data){
			$to_break=false;
			$return_data_array=array();
			foreach ($selector_data["locations_to_visit"] as $url){
				$url=htmlspecialchars_decode($url);//escape any html encoded special charicters
				if(is_a($selector_data["goto"],"shopping_agg\interaction\interaction")){
					$interactional_object=$selector_data["goto"];
					if(isset($selector_data["mode"])&&$selector_data["mode"]=="data"){
						$interactional_object->endpoint->data=array_replace($interactional_object->endpoint->data,array("data_from_response"=>$url));
					}
					elseif (@$selector_data["mode"]["mode"]=="batch"){//if the argumet should be passed as batch
						$selector_data["mode"]["limit"]=isset($selector_data["mode"]["limit"])?$selector_data["mode"]["limit"]:1000;
						$selector_data["mode"]["deliminator"]=isset($selector_data["mode"]["deliminator"])?$selector_data["mode"]["deliminator"]:",";
						if(count($selector_data["locations_to_visit"])>$selector_data["mode"]["limit"]){//if there are too many items to add to the batch call
							\shopping_agg\error_handle\error_log_c("Warning: A batch call has been made with a number of arguments exceeding its limits . The array will be shortened to the lenght of ".(string)$selector_data["mode"]["limit"]." items");
							$selector_data["locations_to_visit"]=array_slice($selector_data["locations_to_visit"],0,$selector_data["mode"]["limit"]);
						}
						$interactional_object->endpoint->data=array_replace($interactional_object->endpoint->data,array("data_from_response"=>implode($selector_data["mode"]["deliminator"],$selector_data["locations_to_visit"])));//add batch call to the new special data item
						$to_break=true;
					}
					else{
						if(!filter_var($url,FILTER_VALIDATE_URL)){//ensure data is a url
							shopping_agg\error_handle\error_log_c("Warning: expected URL at ['$var_index'], got ".$this->vars_to_look_for[$var_index]." instead.",$var_index,$this->vars_to_look_for[$var_index]);
							continue;
						}
						$interactional_object->endpoint->set_endpoint(urldecode($url));//url can be encoded here (no idea why)
					}
				}
				else{//possible bug with data mode. too lazy to fix (deal with it)
					$interactional_object=\shopping_agg\parse\assoc_array_to_to_interactional_object($selector_data["goto"],array("url"=>$this->vars_to_look_for[$var_index]));
				}
				//@todo handle response
				$interactional_object->remap_master($this);
				$return_data=$interactional_object->exec_request();
				
				if(!$return_data){
					shopping_agg\error_handle\error_log_c("Warning: request to $url failed.", $interactional_object);
					if($to_break){//break instead of continuing if batch call was made
						$return_data_array=array();
						break;
					}
					continue;//return empty object
				}
				if($to_break){//break instead of continuing if batch call was made
					$return_data_array=$return_data;
					break;
				}
				array_push($return_data_array,$return_data);//push returned data onto array
				
			}
			if(FLATTEN_MERGE_STRUCTURE){
				array_replace($this->vars_to_look_for, $return_data_array);//update vars_to_look for //FIXME Wont work unless assignment given
				unset($this->vars_to_look_for[$var_index]);//remove reference to that variable
			}
			$final_return_data=array_replace($final_return_data, isset($return_data_array)?$return_data_array:array());
		}
		return $final_return_data;
	}
	/**
	 * This method activly looks for and queues references to exturnal domains in the form array("goto"=>!!!INTERACTIONAL_TREE!!!,"selector"=>$selector)
	 * @param array|string $selector The selector
	 * @param string $key The key of the selector
	 * @param string[] $locations_to_vist an array of the url(s) to visit
	 * @return string $selector The true selector of eiter the url to go to or the item to retrieve.
	 */
	private function queue_data_from_exturnal_resource($selector,$key,$locations_to_vist){
		if(isset($selector["goto"])&&isset($selector["selector"])){//if format for resolving selectors is set up
			if(!isset($this->resource_queue[$key])){//if the array for that key is non exsistant (multiple items for the same attribute)
				$this->resource_queue[$key]=array();//define it as an emptey array
			}
			$this->resource_queue=array_merge($this->resource_queue,array($key=>array("selector"=>$selector["selector"],"goto"=>$selector["goto"],"locations_to_visit"=>$locations_to_vist,"mode"=>isset($selector["mode"])?$selector["mode"]:"url")));//queue item
			return $selector["selector"];//return selector
		}
		return $selector;//return selector
	}
	/**
	 * This function validates an array of variable selector pairings. This excludes CSS3 selectors
	 * @param array $vars_to_look_for The array of var(s) to look for in $key=>$selector pairings.
	 * @return array $vars_to_look_for The validated array of variables to look for
	 */
	private function validate_vars_to_look_for($vars_to_look_for){
		$required=array_keys(unserialize(VARS_TO_LOOK_FOR_REQUIRED));//get the required items
		foreach ($required as $var){
			if(!isset($vars_to_look_for[$var])){//check if the required item exsists
				\shopping_agg\error_handle\error_log_c("Warning: The required field $var is missing from the vars to look for");
			}
		}
		//if ($this->response_type==="JSON"||$this->response_type==="JSONP"){//if the expected response is json
		//	$vars_to_look_for=array_map(function($item){return isset($item["selector"])?$this->validate_json_selector($item["selector"]):$this->validate_json_selector($item);},$vars_to_look_for);//validate json selectors in all cases
		//}
		return $vars_to_look_for;
	}
	/**
	 *  This function tekes in json and uses the "pivot" and "vars_to_look_for" fields to retrieve the key information from it.
	 *  @param string|stdClass|array $json The json response to get varible response from
	 *  @return string[][] This method returns an array of strings.
	 */
	private function get_json_vars_from_response($json){
		if(is_string($json)){
			$json=\shopping_agg\parse\jsonp_decode($json,true);//decode the json string
		}
		elseif(is_a($json, "\stdClass")){
			$json=json_decode(json_encode($json),true);//convert json to an array
		}
		shopping_agg\debug\debug_print("Getting JSON data from response using pivot: $this->pivot");
		$json=eval("return \$json$this->pivot;");
		shopping_agg\debug\debug_print("Data obtained is as follows:$json");
		$new_data=array();
		$current=array();
		foreach ($this->vars_to_look_for as $key=>$vars){
			$vars_orig=$vars;//make backup of selector for later resource queueing
			$vars=isset($vars["selector"])?$vars["selector"]:$vars;//get the true selector of the object if the selector is an exturnal resource
			foreach($json as $item){
				try{$data=eval("return isset(\$item$vars)?\$item$vars:'Index not found.';");}
				catch (\ParseError $e){
					\shopping_agg\error_handle\error_log_c("Error failed to parse variable $key please ensure it is the correct formatting for the json selector.",$vars,$item);
					$data="";
				}
				finally {
					if($data==='Index not found.'||$data===false){
						\shopping_agg\error_handle\error_log_c("Warning: failed to find index of the variable $key in an iteration of the json.",$vars,$item);
					}
					array_push($current,$data);
				}
				
			}
			$this->queue_data_from_exturnal_resource($vars, $key , $current);//pass the selector the key and the current list of url(s)
			$new_data=array_merge($new_data,array($key=>$current));//undate array of values
			$current=array();
		}
		return $new_data;//return data
	}
	/**
	 * This method retrieves the xml from a string and returns the variables as defiend in the selector
	 * @param string $xml The xml to use the jquerey selectors on them
	 * @param string $selector_overide The selecotor to use in recoursive callback
	 * @return string[][] The retrieved data in array("var names"=>array(data)) pairings
	 */
	private function get_vars_from_xml_response($xml,$selector_overide="",$chained=false){
		//if($chained){
			//$doc=new \DOMDocument();
			//$doc->appendChild($xml);
			
		//}
		//else{

		$php_querey=\phpQuery::newDocument($xml);//create xml object
		$xml=$php_querey[$this->pivot];//change focus to pivot
		\shopping_agg\debug\debug_print("Pivot switching focus to ".$this->pivot." of xhr response.",$xml->xml());//give debug printout
		if($xml->xml()==""){
			\shopping_agg\error_handle\error_log_c("Error: Pivot points to non-exsistant location. returning empty array.",$pivot,$xml);
			return array();
		}
			
		//}
		if(DEBUGGING&&DEBUG_SHOW_DATA){
			shopping_agg\debug\debug_print("<b><h1>If you are constructing css3 selectors for item selectors use the array below to build them. (each instance that match will be treated as the attribute of the next item.)</h1></b>");
			echo "<div style='background-color: #66ff66;border-style: dotted;border-width: 5px;border-color: red;'>";
			var_dump(json_decode(json_encode(simplexml_load_string("<root>".str_replace('<?xml version="1.0"?>',"", $xml->xml())."</root>")),true));
			echo "</div>";
		}
		
		$new_data=array();//in event of error
		//$check=array();
		//foreach ($xml as $c){//Because xml cannot be used in the array
		//	echo "<h1>xml</h1>";
		//	var_dump($c);
		//	array_push($check,get_class($c)=="DOMElement");
		//}
		//$check=array_map(function($item){return get_class($xml_s)=="DOMElement";},$xml);
		//if(array_filter($check) == $check){//see pythons any for equivilent (if any item in the $xml object is not a dom element then assume the selection returned more than 1 item meaning chained pivots must be used)
			//$xml=array($xml);
		//	echo "<h1>xml2</h1>";

		//}
		//foreach ($xml as $xml_s){//in event of pivot resolve all child nodes (array length on 1 in most cases)
			//$xml_s=\phpQuery::newDocument($xml_s);

		
		foreach ($this->vars_to_look_for as $key=>$selector){
			$response_handler=null;
			$location_to_look_to="value";
			$selector_orig=$selector;//make backup of selector for queueing exturnal vars later
			if(is_array($selector)||is_string($selector)){//if the selector is not a response object (I.e not an array)
				$selector=isset($selector["selector"])?$selector["selector"]:$selector;//get the selector from both raw string selectors or special extended selectors
				if (!(strpos($selector,">>>>")===false)){//if there was not an error (after looking for '>>>>' notation)
					$location_to_look_to=explode(">>>>",$xml[$selector]);//break at the point where athe data should be located
					$selector=$location_to_look_to[0];//update selector
					$location_to_look_to=$location_to_look_to[1];//get actual required piece
					preg_match('/^\s*(?:(?<data>value)|attribute\s*:\s*((?<du>")|(?<s>\'))(?<attr>(?:(?(du)([^"]|\\\\")*|([^\']|\\\\\')*))(?(du)"|\')))\s*$/x',$location_to_look_to,$matches);//regex with if statements; waaaaat? :o
					if(!empty($matches["data"])){
						$location_to_look_to="value";//if named capture group obtains data as a value assert the value of the xml should be obtained
					}
					elseif(!empty($matches["attr"])){
						$location_to_look_to=$matches["attr"];//if an attribute needs to be matched match that instead
					}
					else{
						error_log_c("Warning: the '>>>>' notation was used on a selector with an invalid format of data location. Please ensure that the end of the selector has '>>>> value' OR '>>>> attribute:'attribute_name'", $location_to_look_to);
						$location_to_look_to="value";
					}
				}
				else{
					$location_to_look_to="value";//if not defined assume value is the item required
				}
			}
			else{
				$response_handler=$selector;//copy selector
				$selector=$response_handler->pivot;//response object in this case
				$response_handler->pivot="";//put the pivot in the wrong place an now have to fix it lol ;_;
			}
			$data=$xml->find($selector);//select data (if data is single entity resolve as single item and not pivot point)[get class so parents are ignored]
			//$stuff=$data->html();
			$data_new=array();
			foreach($data as $item){//convert DOMElement object array into workable text array
				if(!is_null($response_handler)){
					$innerHTML= '';
					$children = $item->childNodes;
					foreach ($children as $child) {
						$innerHTML .= $child->ownerDocument->saveXML( $child );
					}
					//var_dump($innerHTML);
					$data_for_response=\phpQuery::newDocumentXML("<Root>".$innerHTML."</Root>");
					//$php_querey=$php_querey->find("totaloffers");
					$item=$response_handler->get_vars_from_xml_response($data_for_response);
				}
				array_push($data_new,$item);
			}

			$data_new=array($key=>array_map(function ($item)use($location_to_look_to,$selector,$xml,$response_handler,$key){
					if(is_a($item,"\DOMNode")){
					return $location_to_look_to=="value"?$item->nodeValue:$item->getAttribute($location_to_look_to);
					}
					return $item;
			}
			,$data_new));
			if(is_array($selector_orig)||is_string($selector_orig)){
				$this->queue_data_from_exturnal_resource($selector_orig, $key,current($data_new));//queue new objects if they are not functional
			}
			$new_data=array_merge($new_data,$data_new);//Note that item in this contex is a DOMElement object.
		}
		//}
		if(DEBUGGING&&DEBUG_SHOW_DATA){
			echo "<h1> Return data start</h1>";
			var_dump($new_data);
			echo "<h1> return data end</h1>";
		}
		return $new_data;
	}
}