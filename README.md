Aggregation engine
==================

This project aims to simplify the process of interacting with RESTful APIs. This is done by offering an object oriented framework to simplify API interactions.

JSON  files can be used to create interaction objects.

This was originally part of my a452 coursework for A level computer science.

----------


Setup
-------
Download the repository and include `auto_load.php`. This will include all the required files in the correct order. 

For more detailed documentation please reference  [docs](/docs/) section of the repository.