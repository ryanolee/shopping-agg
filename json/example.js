/**
The standard format for API JSON files 
Legend:
{text}<->Replace this with direct value of the variable excluding braces.
[text] OR [text2] OR [text3]<->Replace this with literal contents of one of the square braces(excluding the square braces).
(text)<->Add these as the values for peticular key pairs. They are used as substututes values the PHP needs to know from API responses. (Like item name price ect.)
*/
a={
	API_name:"{name}",//REQUIRED name of API
	API_image:"{URI to image}",//OPTIONAL the source image uri (relative or absolute) for each api source (used for styleization).
	//source_type:"['aggregator'] OR ['shopping']",//OPTIONAL (The type of source that the API relates to(This is more an option to check for duplicate items as items from diffrent api(s) as if an API is connected to an aggregator then there will need to be checks for duplicate sources.))
	//API_endpoint:"{api_endpoint}",//REQUIRED The absolute link To the api end-point. 
	API_requires_auth_key:"[true] OR [false]",//REQUIRED (Tells the api if there is the requirement of an api key or not)
	search:s,//REQUIRED (Please refer to the interaction constuct for dealing with xhr reqests)
	api_keys:{//REQUIRED (if API_requires_auth_key is true) outlines the api key(s) and how to use it/them.
		x:{//REQUIRED(the name of the key) Where x is the name of the key and is IS THE FIELD NAME FOR THE API KEY BY DEFAULT
			method:"['POST'] OR ['GET'] OR ect...",//REQUIRED (if API_requires_auth_key is true) The method on how to send the api key
			name:"{key_name}",//OPTIONAL (The name of the api key) This will overide the value given to the value that the JSON object is assigned to and act as the new field name for the API key.
			value:"{key_value}",//REQUIRED (the value of the API key) This is the place where the value for the api key will be given.
			default_key:"[true] OR [false]"//REQUIRED (whether this key is the default api key) This key will allways be appended to any/and or all requests related to this API.	
		}//there mey be multiple keys given but an error will be thrown if there are more than two default keys defined.
	}
}
s={//interaction construct(This represents the standard format for how requests should be sent to the server)
		request:{//The area where the request to the server is defined.
			endpoint:"{endpoint}",//REQUIRED This is where the request enpoint is defined.
			method:"{Method}",//OPTINAL default GET the methpod the script should deploy.
			API_key:"{Api_key}",//OPTIONAL the api key to use for requests.
			//In these data segments for the value of a variable you can put "SUPERGLOBAL:'index'(only GET or POST (not REQUEST -to be added))" to access superglobals;
			//e.g {search:"POST:'search'"} -> array("search"=>$_POST['search'])	
			//This also works for functions defined in ./funcs/extra.php by the way of {"price":"function:'price('float')'"} -> array(price=>/shopping_agg/extra/price('float'))
			//You can also reference pre-exsisting variables e.g {"data":{"test":"hello"},post_data{"test_2":"data:'test'"}} -> array("post_data"=>array("test2"=>"test")) as it holds a reference to data:'test' (This works between "data","post_data" and "get_data")
			//The syntax being "data_store:'data_name' precedence:'intiger'"
			//You can also define a precedence for when the data should be resolved (the lower the prirority the faster it will be resolved)
			//A more complex (valid call would be){"var":"function:'get_key(GET:\'data\',function:\'get_time_of_request(post_data:\'time_of_request\')\')'"}
			//NOTE only functions hold this synatx so you cannot do  {"var":"GET:'function:\'time()\''"}
			//This also works for a line of php code that will five a retun value
			data:{},//REQUIRED the data to send to the API as either form fields or as a querey string (Default is method ther method defind in the request statement.)
			POST_data:{},//OPTINAL will be combined with the original data object if method is the same or forcably send itself as that spcific method type
			GET_data:{},//OPTINAL will be combined with original data object if  the data is the same.
			
		},
		response:{//This is where the planned response data is implementd
			"response_type":"(xml)OR(json)",//This is where you define the type of resposne data expected (xml,json,jsonop) Please note that jsonop and json are both taken to be the same.
			vars_to_look_for:{//The vars to look for segment follows a path of varname var identifier.
				var_name:"(var_identifier)OR...",//the var name is how variable reference in php and var identifier to.
				//Var identifier documentation;
				//If you response is xml:
				//Use css3 selector to get to the list of items:
					//Afterwich...
					//you can select items from the array using ["price"],["quantaty"],["description"] ect...
					//If data is stored in the atributes of an item use a ":"
					//e.g 
					//<foo>
					//	<baz>
					//		<bar>
					//			<item>
					//				<price>
					//					1.00
					//				</price>
					//				...
					//			</item>
					//			<item>
					//				<price>
					//					1.50
					//				</price>
					//				...
					//			</item>
					//			<item>
					//				<price>
					//					3.00
					//				</price>
					//				...
					//			</item>
					//		</bar>
					//	</baz>
					//</foo>
					//would require
					//item_price:{
					//	selector:"foo > bar > baz > item > price"
					//}
					//NOTE: each variable needs to have its own selector.
					//		Items will be built off the selectors so each selector needs to return an array of items where the index is representative of the item.
					//		items are defined based off the index of each variable.
				//with json:
					// It functions off a metod similar to that listed above. This method expects an array of items
					//e.g
					//{foo:
						//baz:{
							//bar:[{price:"1"},{price:"2"},{price:"3"}]
						//}
					//}
					//would require:
					//{
					//type:"json"
					//pivot:"['foo']['baz']['bar']",
					//vars_to_look_for:{
					//		price:"['price']"
					//	}
					//}
					//You must first select a pivot that acts as the root for item selection and is relient off string index selection.
					//(a path of indexses from the outer json to the array that the items are stored in)
					//From there reserved item values may be retained.
					//(?:\['(?<name>[^']*)'\])
					//OR
				var_name:{
					selector:"As above but must lead to a url",//This is a special extra case allows for a variable to go and get data from an exturnal resource. This selector must lead to a valid url.
					goto:{s}//the object contained within GOTO must be another instance of the search object tree (url overwitten with url found at selector).
				}
			},
			errors:{//REQUIRED Thease are the possible expected errors that could arise from the api. These are needed so that they can be handled an logged properly.
				error_x:{//REQIRED There can be more then one format of error but errors must only be defined is there format is not alredy done so.
					//NOTE (refer to (var_identifier) for the addessing system) witch all of thease follow
					error_name:"(Error_name)",//OPTIONAL This is reprenatative of the location of names for each error .
					error_code:"(Error_code)",//OPTIONAL this should represent the location of the error code for each error.
					error_message:"(Error_message)",//REQUIRED this should represent the location of the error message for each error.
					error_type:"(Error_message)"//OPTIONAL this should represent the location of the error type for each error.
				}
			}
		}	
}
/*WITHOUT DOCUMENTATION FOR VISULIZATION
{
	"API_name":"{name}",
	"API_image":"{URI to image}",
	"source_type":"['aggregator'] OR ['shopping']",
	"api_endpoint":"{api_endpoint}", 
	"API_requires_auth_key":"[true] OR [false]",
	"search":{
		"request":{
			"endpoint":"{endpoint}",
			"method":"{Method}",
			"data":{},
			"POST_data":{},
			"GET_data":{}
			
		},
		"response":{
			"response_type":"(xml)OR(json)",
			"vars_to_look_for":{
				"var_name":"(var_identifier)"
			}
		},
		"errors":[]

},
	"errors":{
		"error":{
			"error_name":"(Error_name)",
			"error_code":"(Error_code)",
			"error_message":"(Error_message)",
			"error_type":"(Error_message)"
		}
	},
	"api_keys":{
		"x":{
			"method":"['POST'] OR ['GET'] OR ect...",
			"name":"{key_name}",
			"value":"{key_value}",
			"default_key":"[true] OR [false]"
		}
	}
}


*/


