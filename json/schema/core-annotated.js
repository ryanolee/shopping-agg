{  
   "type":"object",//match a json object
   "properties":{  //with the properties of
      "API_name":{  //api_name
         "type":"string",//that must be a string
         "description":"The name of the api."
      },
      "API_image":{  //api_image
         "type":"string",//that must be a string
         "description":"A url the the image of the website this aggragator is representing."
      },
      "API_requires_auth_key":{  //api_image
         "type":"boolean",//that must be a boolean
         "description":"If the api requires api keys. (or should use the inbuilt api key mechanism)"
      },
      "api_keys":{  //api_keys
         "type":"object",//That must be an object (not required)
         "description":"any given api key",
         "properties":{  //with no properties

         },
         "additionalProperties":{  //and any extra properties must be...
            "type":"object",//an object
            "properties":{  //with the properties of
               "method":{  //method
                  "type":"string",//that must be a string
                  "pattern":"^(?:POST|GET)$",// and match POST or GET
                  "description":"The method the api key should be sent over."
               },
               "name":{  //name
                  "type":"string",//that must be a string
                  "description":"The name of the api key."
               },
               "value":{  //value
                  "type":"string",//that must be a string
                  "description":"The value of the api key"
               },
               "default_key":{  //default_key
                  "type":"boolean",//that must be a string
                  "description":"If this is the default API key or not."
               }
            },
            "required":[  //the properties method, value and name are absolutly required for this object.
               "method",
               "name",
               "value"
            ]
         }
      },
      "search":{//search
        "$schema":"search",//define this as a subscema
         "type":"object",//that matches an object
         "description":"The main object holding information relating to initiatiating a search querey to the api",
         "properties":{  //with the properties of
            "request":{  //request
               "type":"object",//that in itself must be an object
               "description":"This is a standard interactional construct for api requests.",
               "properties":{  //with the properties of
                  "endpoint":{//endpoint  
                     "type":"string",//that must be a string
                     "description":"The full absolute URL to the api endpoint"
                  },
   				  "API_key":{  //api_key
                     "type":"string",//that must be a string
                     "description":"The name of the api key to use as defined in the api_keys object"
                  },
                  "pre_request_function":{//pre_request_function [ADDED LATER]
                  	 "type":"string",//that must be a string
                  	 "description":"The name of the pre-request function to call to manipulate the final url."
                  },
                  "method":{  //method
                     "type":"string",//that must be a string
                     "pattern":"^(?:POST|GET|HEAD|DELETE|CONNECT|OPTIONS|PUT|TRACE)$",//that must be POST,GET,HEAD,DELETE,CONNECT,OPTIONS,PUT or TRACE
                     "description":"The method to send the request as. "
                  },
                  "data":{  //data
                     "type":"object",//that must be an object
                     "description":"The data that will be sent over the default data send method (POST or GET)",
                     "properties":{  

                     },
                     "additionalProperties":{  
                        "type":"string"//where all properties are strings
                     }
                  },
                  "POST_data":{  //POST_data
                     "type":"object",
                     "description":"Data that will be sent exclusivly as form fields. (POST)",
                     "properties":{  

                     },
                     "additionalProperties":{  
                        "type":"string"//where all properties are strings
                     }
                  },
                  "GET_data":{  //get_data
                     "type":"object",
                     "properties":{  

                     },
                     "additionalProperties":{  
                        "type":"string"//where all properties are strings
                     },
                     "description":"Data that will be sent exclusivly as a querey string. (GET)"
                  }
               },
               "required":[ //Where the properties endpoint and method are absolutly required 
                  "endpoint",
                  "method"
               ]
            },
            "response":{  //response...
               "type":"object",//that in itself is an object
               "description":"This is the object that holds all of the data to parse an api response",
               "properties":{  //with the properties of
                  "response_type":{  //response type
                     "type":"string",//that must be a string
					 "pattern":"^(?:JSON|XML|Json|Xml|xml|json)$",//and match the type JSON XML (case insenstaive)
                     "description":"The expected type of data to be returned from any given api. (JSON or XML)"
                  },
                  "pivot":{  //pivot
                     "type":"string",//that must be a string
					 "pattern":"^\\s*(?:\\s*\\[\\s*(?:\\\"[^\\\"]*\\\"|'[^']*'|\\d+)\\s*\\])+\\s*$",//an match the form ['index1']['index2']['index3']
                     "description":"Where the array of items is located (Only valid for JSON return types)"
                  },
                  "vars_to_look_for":{  //vars_to_look_for
                     "type":"object",// That in itself is an object
                     "description":"The variables to look for with it's related identifier",
                     "properties":{  
                     },
                     "additionalProperties":{//That's properties must be..
                          "oneOf":[//one of
							{
                              "type":"object",//an object with 2 properties (where all properties are required)
							  "description":"This is a special object to represent chainable response types that ultimatly flatten to normalizable objects",
                              "properties":{
                              	"goto":{"$ref": "#/properties/search"},//the first of witch must be an interactional tree (circualr reference made using sub schema)
								"selector":{"type":"string"}//the second of witch must be a string
                              },
                              "minProperties": 2,
  							  "maxProperties": 2,
                              "required":[
                              	"goto",
                                "selector"
                              ]
							},
							{
							"type":"string"//or just a string (as an aelector)
							}
							]
                     }
                  }
               },
               "required":[  //where response type and vars_to_look_for is required to be defined
                  "response_type",
                  "vars_to_look_for"
               ]
            },
            "errors":{//errors
				"description":"This is the array of errors as defined in root.errors and references possible errors that should be checked for as a result of this error.",
				"type": "array",//that is an array
				"minItems": 1,//That must have atleast 1 item 
				"items": { "type": "string" },//Where all items in the array must be strings
				"uniqueItems": true//and unique
			}
         },
         "required":[  //request, respone and errors are required.
            "request",
            "response",
            "errors"
         ],
         "additionalProperties":false//and no additional properties are allowed
      },
	  "errors":{  //errors
               "type":"object",//that is an object
               "description":"All the errors the aggregator needs to look out for from this api.",
               "properties":{  

               },
               "additionalProperties":{  //where each property must...
                  "type":"object",//be an object
                  "description":"The normal format for an error",
                  "properties":{  //that has the properties
                     "error_name":{  //error_name
                        "type":"string",//that must be a string
                        "description":"Link to the name of the error"
                     },
                     "error_code":{  //error_code
                        "type":"string",//that must be a string
                        "description":"Link to the error code"
                     },
                     "error_message":{  //error_message
                        "type":"string",//that must be a string
                        "description":"Link to the error message"
                     },
                     "error_type":{  //and error_type
                        "type":"string",//that must be a string
                        "description":"Link to the type of error"
                     }
                  },
                  "required":[  //where only error_message is required.
                     "error_message"
                  ],
                  "additionalProperties":false//and no additional properties are allowed
               }
            }
   },
   "required":[  //where api_name , api_requires_auth_key and search are required.
      "API_name",
      "API_requires_auth_key",
      "search"
   ],
   "additionalProperties":false//and no aditional properties are allowed.
}